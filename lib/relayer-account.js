/*
access granted to ACCOUNT_OWNER only
requests are rewritten to /accountapi/uid/...  and also filtered


router.route("/account/:account_id/domains")
   .get(expressGetAccount(function(account, req,res,next){
router.post("/account/:account_id/email/confirmation/verify",
router.post("/account/:account_id/email/confirmation/send",

router.route("/account/:account_id/emails")
   .put(expressGetAccountTrx(function(trx, account, req,res,next) {
   .get(expressGetAccount(function(account, req,res,next) {

router.post("/account/:account_id/change_password",

ACCOUNT_FORCE_PASSWORD credential (not even ACCOUNT_OWNER)
   router.post("/account/:account_id/force_password",

router.route("/account/:account_id")
  * .get(expressGetAccount(function(account, req,res,next) {
  * .post(expressGetAccountTrx(function(trx, account, req,res,next) {

router.post("/account/:account_id/email/:email/send/registration", function (req, res, next) {
router.post("/account/:account_id/email/:email/send/confirmation", function (req, res, next) {
router.post("/account/:account_id/email/:email/verify", function (req, res, next) {
router.post("/account/:account_id/email/:email/set_primary", function (req, res, next) {
router.delete("/account/:account_id/email/:email",function (req, res, next) {

router.post("/account/:account_id/account_grant",expressGetAccountTrx(function(trx, mainAccount, req,res,next) {
router.get("/account/:account_id/account_grants/for",expressGetAccount(function(account, req,res,next){
router.get("/account/:account_id/account_grants/to",expressGetAccount(function(account, req,res,next){

*/

module.exports = function(app) {

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const allowedSubPermissions = [ "ACCOUNT_DNS" ]

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()

  var vali = require("MonsterValidators").ValidateJs()

  const grantHelper = require("grant-helper.js")(app);

  var RateLimit = require("RateLimit")

  var loginLimiter

  var limiter = new RateLimit({
    windowMs: app.config.get("account_ratelimit_window_sec") *1000, // 15 minutes
    max: app.config.get("account_ratelimit_max"), // limit each IP to 100 requests per windowMs
  });

  grantHelper.configureCaptchaRoutes(router);


  router.post("/config/webhostingdomains/txtrecords/ex", function(req,res,next){
     return vali.async(req.body.json, {server:{presence:true, isString:{strictName: true}}})
      .then(d=>{
          const dns = require("dns");
          dns.lookup(d.server+"."+app.config.get("service_primary_domain"), (err, address, family) => {
             if(err) return next(err);
             req.method = "GET";
             app.Relay("ServersAccountapi", "/accountapi/config/webhostingdomains/txtrecords", req, function(err){
                   if(err) return next(err);
                   req.result.a_record = address;
                   return req.sendResponse(req.result);
             })

          });

      })

  })

  router.get("/config/webhostingdomains/txtrecords", function(req,res,next){

      app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next)
  })

  router.post("/force_password",function(req,res,next){
     if(!req.TokenUser.session.IsGranted("ACCOUNT_FORCE_PASSWORD")) return next(new MError("PERMISSION_DENIED"))

     rewriteAndRelay(req, next)
  })

  router.put("/loginlog/events",function(req,res,next){

      if(req.TokenUser.session.IsGranted("LOGINLOG_EVENT_REPORT")) {
         app.assignOriginatingServer(req);
         return app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next)
      }

      next();
  })

  router.use(function(req,res,next){

     if(req.result) return next()

     if(!req.TokenUser.session.IsGranted("ACCOUNT_OWNER")) return next(new MError("PERMISSION_DENIED"))

     next()
  })

  function forceOwnLoginsOnly(req,res,next){
     req.body.json.hide_superuser = true;
     return next();
  }
  router.post("/loginlog/count", forceOwnLoginsOnly);
  router.post("/loginlog/search", forceOwnLoginsOnly);

  function flushTokenEverywhere(req){
      return app.DeleteTokenFromCache(req, true)
            .then(()=>{
                 return app.ServersLib.GetAllServers()              
            })
            .then(servers=>{
                var relayer = app.getMapiRelayer(req);
                const this_server_name = app.config.get("server_name");
                const dotq = require('MonsterDotq');
                return dotq.linearMap({array:servers, action: server=>{
                    var a_server_name = server.s_name;
                    if(a_server_name == this_server_name) return; // skipping ourselves (did it directly already)

                    console.log("sending flush request to", server);
                    return relayer.postAsync("/s/"+a_server_name+"/su/flush/token", {});
                }})
            })
            .then(()=>{
               console.log("Cache invalidated!");
            })
  }

  router.post("/flush/token", function(req,res,next){
     return req.sendOk(flushTokenEverywhere(req));
  });

  // note: we remove the token from the cache, as the put request will most probably 
  // insert a new authorized domain to the account
  // this is an exception: authorization takes place inside MonsterAccount!
  router.put("/webhostingdomains", function(req,res,next){
     rewriteAndRelay(req, function(err){
        if(err) return next(err);

        console.log("New webhostingdomain has been added, invalidating cache");
        return flushTokenEverywhere(req)
          .then(()=>{
              return req.sendResponse(req.result);            
          })
          .catch(next);
     })
  })


  var grantedCalls = {
    "get": [
       "/domains",
       "/webhostings",
       "/webhostingdomains",
       "/emails",
       "/tels",
       "/credentials",
       "/credential/:cred_user",
       "/account_grants/to",
       "/account_grants/for",

       "/tel/:tel",

    ],
    "put": [
       "/tels",
    ],
    "post": [
        "/account_grant",
        "/email/confirmation/send",
        "/change_password",

        "/totp/status",
        "/totp/activate",
        "/totp/inactivate",

        "/loginlog/count",
        "/loginlog/search",

        "/email/:email/send/registration",
        "/email/:email/send/confirmation",
        "/email/:email/verify",
        "/email/:email/set_primary",

        "/tel/:tel/set_primary",

        "/callback_request",

        "/webhostingdomains/txt-record-code/",
    ],
    "delete": [
        "/email/:email",
        "/tel/:tel",
        "/credential/:cred_user",
        "/account_grant",
    ]
  }

  for(var method of Object.keys(grantedCalls)) {
     for (var call of grantedCalls[method]) {
        router[method](call, function(req,res,next){
           rewriteAndRelay(req,next)
        })

     }
  }

  Array("/tel/:tel/verify", "/tel/:tel/send").forEach(uri=>{
     router.post(uri, limiter, function(req,res,next){
       return grantHelper.captchaEnforceAsync("-", req)
         .then(()=>{

             // and relay
             rewriteAndRelay(req,next)

         })

     })
  })


   router.route("/webhostingdomains/")
     .delete(function(req,res,next){
         // -------------------------------- TODO: check whether everything has been removed previously
          rewriteAndRelay(req,next)
       }
     )



  function getAllowedSubpermissions(){
     return allowedSubPermissions
  }
  router.get("/credentials/supported",function(req,res,next){

      req.sendResponse(getAllowedSubpermissions());
  })


  function expressCredentialBusinessLogic(){

      return function(req,res,next){

          if(!req.TokenUser.session.IsAnyGranted(["ADMIN_ACCOUNTAPI","SUPERUSER"]))
             delete req.body.json.c_server;


          return vali.async(req.body.json, {c_grants: { inclusionCombo: getAllowedSubpermissions() }})
            .then(()=>{
               rewriteAndRelay(req, next)
            })

      }
  }

  router.put("/credentials",expressCredentialBusinessLogic())
  router.post("/credential/:cred_user",expressCredentialBusinessLogic())


  router.post("/switch_user",function(req,res,next){


      var d
      return vali.async(req.body.json, {account_id: { presence: true, isString: true }})
        .then((ad)=>{
           d = ad

           var mapi = app.GetMapiPool("ServersAccountapi")
           return mapi.getAsync("/accountapi/account/"+ req.TokenUser.account.u_id + "/account_grants/for")
        })
        .then(re=>{

           for (var q of re.result) {
              if(q.u_id == d.account_id) return Promise.resolve(q)
           }
           throw new MError("PERMISSION_DENIED")
        })
        .then(()=>{

           d.on_behalf = req.TokenUser.account.u_id

           // we use custom payload here, so we can be certain that only the account parameter will be relayed
           app.Relay("ServersAccountapi", "/accountapi/authentication/switch_user", req, next, d)
        })

  });


  router.put("/emails",function(req,res,next){
     delete req.body.json.e_confirmed
     delete req.body.json.e_primary

     rewriteAndRelay(req, next)
  })

  router.post("/email/confirmation/verify",function(req,res,next){
     rewriteAndRelay(req,function(err){
        if(err) return next(err)

        // since the verification status has probably changed, session caches should be invalidated
        app.DeleteTokenFromCache(req)
        next()
     })
  })

  router.get("/",function(req,res,next){
     rewriteAndRelay(req,function(err){
        if(err) return next(err)

        // this is hidden
         delete req.result.u_comment
         delete req.result.u_primary_email_stripped

        next()
     })
  })
  router.post("/",function(req,res,next){
     // change details, should be filtered
     var d = {}
     for(var q of ["u_name", "u_email_login_ok_subsystems", "u_email_login_fail", "u_email_login_ok", "u_language","u_callin", "u_sms_notification"]) {
       d[q] = req.body.json[q]
     }
     req.body.json = d
     rewriteAndRelay(req, next)
  })



  router.use(function(req,res,next){

    if(req.result) return next()

    return next(new MError("PERMISSION_DENIED"))

  })


  return router

  function rewriteAndRelay(req, next) {
      var aPath = "/accountapi/account/"+ req.TokenUser.account.u_id + req.path

      app.Relay("ServersAccountapi", aPath, req, next)

  }
}
