module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use("/docrootapi", require("relayer-docrootapi.js")(app))

  router.use("/git", require("relayer-git.js")(app))

  router.use("/webhosting", require("relayer-webhosting.js")(app))

  router.use("/iptables", require("relayer-iptables.js")(app))

  router.use("/dbms", require("relayer-dbms.js")(app))

  router.use("/eventapi", require("relayer-eventapi.js")(app))

  router.use("/fileman", require("relayer-fileman.js")(app))

  router.use("/ftp", require("relayer-ftp.js")(app))

  router.use("/mailer", require("relayer-mailer.js")(app))

  router.use("/email/pub", require("relayer-email-pub.js")(app))

  router.use("/email", require("relayer-email.js")(app))

  router.use("/geoip", require("relayer-geoip.js")(app));

  router.use("/cert", require("relayer-cert.js")(app))

  router.use("/docker", require("relayer-docker.js")(app))

  router.use("/wizard", require("relayer-server-specific-wizard.js")(app))

  router.use("/info/webhosting", require("relayer-info-webhosting.js")(app)) // this is always local server only

  router.use("/test", require("relayer-test.js")(app))


  router.post("/su/flush/token", function(req,res,next){

      app.DeleteTokenFromCache(req)
      req.sendOk()
  })

  router.use("/su/editor", require("relayer-editor.js")(app));

  router.use("/su/sync", require("relayer-sync-internal.js")(app))

  router.use("/su/wie", require("relayer-wie.js")(app));

  router.use("/su/timemachine", require("relayer-timemachine.js")(app));

  var SuLib = require("relayer-su.js")
  SuLib(app, router, "eventapi",    "ServersEvent",       ["ADMIN_EVENTAPI"])
  SuLib(app, router, "git",         "ServersGit",         ["ADMIN_GIT"])
  SuLib(app, router, "mailer",      "ServersMailer",      ["ADMIN_MAILER"])
  SuLib(app, router, "docrootapi",  "ServersDocrootapi",  ["ADMIN_DOCROOTAPI"])
  SuLib(app, router, "webhosting",  "ServersWebhosting",  ["ADMIN_WEBHOSTING"])
  SuLib(app, router, "dbms",        "ServersDbms",        ["ADMIN_DBMS"])
  SuLib(app, router, "fileman",     "ServersFileman",     ["ADMIN_FILEMAN"])
  SuLib(app, router, "ftp",         "ServersFtp",         ["ADMIN_FTP"])
  SuLib(app, router, "installatron","ServersInstallatron",["ADMIN_INSTALLATRON"])
  SuLib(app, router, "email",       "ServersEmail",       ["ADMIN_EMAIL"])
  SuLib(app, router, "cert",        "ServersCert",        ["ADMIN_CERT"])
  SuLib(app, router, "iptables",    "ServersIptables",    ["ADMIN_IPTABLES"])
  SuLib(app, router, "tapi",        "ServersTapi",        ["ADMIN_TAPI"])
  SuLib(app, router, "docker",      "ServersDocker",      ["ADMIN_DOCKER"])

  return router

}
