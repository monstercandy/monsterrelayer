module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")(app);


  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersEmail", '/email')


  router.use(function(req,res,next){

     if((req.body)&&(req.body.json)) {
        if(!req.TokenUser.session.IsAnyGranted(["ADMIN_EMAIL","SUPERUSER","SERVER_OWNER"])) {
          delete req.body.json.el_verified
          delete req.body.json.bc_verified          
        }
        delete req.body.json.skip_maildirmake
     }

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_EMAIL")) req.granted = true

     next()
  })

  Array("put","get","delete").forEach(n=>{
     router[n]("/dkim/:domain", relayIfGrantedDomain);
  })

  router.put("/domains",
    function(req,res,next){
      req.params.domain = req.body.json.do_domain_name
      next()
    },
    assertWebhosting,
    relayIfGrantedDomain,
    function(req,res,next){
      if((!req.granted)&&(req.body.json.do_webhosting)) {
        // look for service domains
        var this_server_name = app.config.get("server_name");
        var service_account_tld = app.config.get("email_service_account_tld");
        if(req.body.json.do_domain_name == req.body.json.do_webhosting+"."+this_server_name+service_account_tld)
           req.granted = true;
      }
      next();
    }
  )
  router.search("/domains/domain/:domain",relayIfGrantedDomain)
  router.post("/domains/domain/:domain", assertWebhosting, relayIfGrantedDomain, function(req,res,next){
     delete req.body.json.do_suspended;
     next();
  })
  router.post("/domains/domain/:domain/greylisting",relayIfGrantedDomain)

  router.post("/accounts/mail-service-account/fetch", function(req,res,next){
     if(req.TokenUser.session.IsAnyGranted(["MAIL_SERVICE_PASSWORD_LOOKUP"])) {
        req.granted = true;
     }
     next();
  });

  router.post([
      "/accounts/list",
      "/aliases/list",
      "/bccs/list",
    ],
    enforceAccountParameter,
    function(req,res,next){
       req.granted = true
       next()
    }
  )
  router.post(
    "/webhostings/:webhosting/list",
    enforceAccountParameter,
    relayIfGrantedWebhosting
  )

  Array("accounts","aliases","bccs").forEach(n=>{
    router.get("/webhostings/:webhosting/"+n, relayIfGrantedWebhosting);
  })

  router.post("/webhostings/:webhosting/domains/domain/:domain/account/:account",function(req,res,next){
     if(typeof req.body.json.ea_suspended != "undefined")
       throw new MError("EMAIL_ACCOUNT_SUSPENDED");

     return next();
  })
      

  router.get("/postfix/mailq/by/domain/:domain", relayIfGrantedDomain);
  router.get("/postfix/mailq/by/webhosting/:webhosting", relayIfGrantedWebhosting);


//a valid uri will look like: /api/s/s1/email/webhostings/16433/domains/domain/somedomain.hu/accounts
  Array(
      "/webhostings/:webhosting/domains",
      "/webhostings/:webhosting/domains/domain/:domain",
      "/webhostings/:webhosting/domains/domain/:domain/aliases",
      "/webhostings/:webhosting/domains/domain/:domain/alias/:alias",
      "/webhostings/:webhosting/domains/domain/:domain/alias/:alias/verify",
      "/webhostings/:webhosting/domains/domain/:domain/accounts",
      "/webhostings/:webhosting/domains/domain/:domain/account/:account",
      "/webhostings/:webhosting/domains/domain/:domain/account/:account/cluebringer/quotas",
      "/webhostings/:webhosting/domains/domain/:domain/account/:account/cluebringer/sessions",
      "/webhostings/:webhosting/domains/domain/:domain/account/:account/cleanup",
      "/webhostings/:webhosting/domains/domain/:domain/bccs",
      "/webhostings/:webhosting/domains/domain/:domain/bcc/:bcc",
      "/webhostings/:webhosting/domains/domain/:domain/bcc/:bcc/verify"
  ).forEach(function(route){

     router.all(route, relayIfGrantedWebhosting)

  })


  router.post("/webhostings/list", function(req,res,next){
     if(typeof req.body.json != "object")
       req.body.json = {}


     // this step is redundant here but we dont want to forget
     req.body.json.account = req.TokenUser.account.u_id

     req.granted = true
     next()

  })

  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     if(typeof req.body.json == "object")
        req.body.json.account = req.TokenUser.account.u_id

     return app.Relay("ServersEmail", "/email"+req.path, req, next)
  })

  function relayIfGrantedWebhosting(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
  }

  function relayIfGrantedDomain(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perDomainGrant)(req,res,next)
  }

  function enforceAccountParameter(req,res,next){
      if(!req.body.json) req.body.json = {}
      req.body.json.account = req.TokenUser.account.u_id
  }

  function assertWebhosting(req, res, next) {
      var wh_id = req.body.json.do_webhosting || 0;
      if(wh_id) {

         if(!GrantHelper.doesWebhostingBelongToUser(req, req.params.server, wh_id))
           throw new MError("PERMISSION_DENIED");

      }

      return next();
  }

  return router

}
