/*
SUPERUSER
ADMIN_TAPI
ACCOUNT_OWNER
ACCOUNT_DNS

		router.route( "/soa-contact")
		get / post / delete

		router.route( "/domains")
		   search / get / put

		router.post( "/build", function (req, res, next) {

		router.post( "/import/tgz", function (req, res, next) {

		router.get( "/export/tgz", function (req, res, next) {

router.get( '/domain/:domain/tinydnsdata', function(req,res,next){

router.route( '/domain/:domain')
   search / post / put / get / delete

router.delete( '/domain/:domain/:record', function(req, res, next) {


*/

module.exports = function(app) {

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()

  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_TAPI")) req.granted = true


     next()
  })

  router.all("/domain/:domain/:record", perDomainGrant)
  router.all("/domain/:domain", soa_filter, perDomainGrant)

  router.post("/build",function(req,res,next){
     if((!req.granted) && (req.TokenUser.session.IsGranted("TAPI_BUILD")))
        req.granted = true

     next()
  })

  router.use(function(req,res,next){
  	 if(!req.granted)
  	  	return next(new MError("PERMISSION_DENIED"))

     app.Relay("ServersTapi", "/tapi"+req.path, req, next)
  })


  return router

  function soa_filter(req,res,next) {
      if(req.granted) return next();
      if(app.config.get("tapi_non_privileged_soa_edit")) return next()
      if((!req.method.match(/^(POST|PUT|DELETE)$/))) return next()

      var d = req.body.json
      if(!Array.isArray(d)) d = [d]

      var soa_found = false
      d.some((entry)=>{
          if((entry.type)&&(entry.type.match(/^(SOA|SOA\/NS\/A|NS\/A)$/))){
             soa_found = true
             return true
          }
      })

      if(soa_found) return next(new MError("PERMISSION_DENIED"))

      next()
  }


   function  perDomainGrant(req,res,next){
  	  if(req.granted) return next()

  	  if(!req.TokenUser.session.IsAnyGranted(["ACCOUNT_OWNER","ACCOUNT_DNS"])) return next()

      for(var q of req.TokenUser.domains) {
      	 if(q.d_domain_canonical_name == req.params.domain){
      	 	req.granted = true
      	 	break;
      	 }
      }

      next()
  }

}
