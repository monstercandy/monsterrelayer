module.exports = function(recaptchaParams){
	 if(!recaptchaParams.secretkey) throw new Error("Mandatory argument secretkey is missing");

	 const request = require("RequestCommon");
	 const MError = require("MonsterError");

	 var lib = {};
	 lib.VerifyAsync = function(userResponse, remote_ip){
	 	return request.PostForm("https://www.google.com/recaptcha/api/siteverify", {secret: recaptchaParams.secretkey, response: userResponse, remoteip: remote_ip}, {asJson: true})
	 	  .then((d)=>{
	 	  	  if(!d.success)
	 	  	  	throw new MError("CAPTCHA_MISMATCH");
	 	  })
	 }
	 return lib;
}
