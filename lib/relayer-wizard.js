module.exports = function(app) {

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const MError = require('MonsterError')

  var vali = require("MonsterValidators").ValidateJs()

  const domainsCanBeAddedFreely = app.config.get("webhosting_domains_can_be_added_without_txt_records");

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){
     // this is a local stuff, relaying to this MonsterRelayer instance using the token from the original request
     req.relayerWithTokenFromOriginalRequest = app.getMapiRelayer(req)

     if(req.TokenUser.session.IsSuperUser()) req.granted = true;

     next()
  })


  function dkimAction(callback){
      return function(req,res,next) {
          if(!req.granted) return next();

          return req.relayerWithTokenFromOriginalRequest[req.method.toLowerCase()+"Async"]("/s/"+req.params.server+"/email/dkim/"+req.params.domain, req.body.json)
           .then(re=>{
             req.result = re.result;

             console.log("relay operation to dkim succeeded", req.method, req.path, req.result);
             
             // dkim operation was successful
             return findDnsZone(req, req.params.domain, "")
               .then(r=>{
                  req.dnsZone = r;
                  return callback(req,res,next);
               })

          })
          .catch(ex=>{
             return next(ex);
          })
      }


      function findDnsZone(req, currentZoneName, additionalHostPrefix){
         return req.relayerWithTokenFromOriginalRequest.getAsync("/tapi/domain/"+currentZoneName)
           .then(function(re){
              // zone found!
              return {
                 dnsZone: currentZoneName,
                 additionalHostPrefix: additionalHostPrefix,
              };
           })
           .catch(function(ex){
              console.error("DNS zone", currentZoneName, "does not seem to exist")
              var components = currentZoneName.split(".");
              if(components.length <= 2) {
                 console.error("No more components, giving up");
                 return;
              }
              if(additionalHostPrefix)
                 additionalHostPrefix += ".";
              var fc = components.shift();
              additionalHostPrefix += fc;
              var newHostname = components.join(".");
              return findDnsZone(req, newHostname, additionalHostPrefix);
           })
      }
  }
  function getHostPrefix(host_prefix, dnsZone){
     var host = host_prefix;
     if((dnsZone)&&(dnsZone.additionalHostPrefix)) {
        host += "."+dnsZone.additionalHostPrefix;
     }
     return host;
  }


  router.get("/dkim/:server/:domain", function(req,res,next){
      return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+req.params.server+"/email/dkim/"+req.params.domain)
        .then(dre=>{
            return req.sendResponse(dre.result);
        })
  });


  router.put("/dkim/:server/:domain", dkimAction(function(req,res,next){
      req.result.dnsSuccess = false;

      if(!req.dnsZone) return req.sendResponse(req.result);

      var details = req.result;

      var host = getHostPrefix(details.host_prefix, req.dnsZone);

      return req.relayerWithTokenFromOriginalRequest.putAsync("/tapi/domain/"+req.dnsZone.dnsZone, {
         host: host,
         type: "TXT",
         text: details.raw_txt_value,
      })
      .then(()=>{
         req.result.dnsSuccess = true;
      })
      .catch(function(ex){
        console.error("unable to add dns records for dkim", ex)
      })
      .then(()=>{
         return req.sendResponse(req.result);
      })
  }));
  router.delete("/dkim/:server/:domain", function(req,res,next){
     // fetching the existing record details
      if(!req.granted) return next();

      return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+req.params.server+"/email/dkim/"+req.params.domain)
      .then(re=>{
         req.dkim = re.result;
         console.log("DKIM record details queried", req.dkim);
         next();
      })
      .catch(ex=>{
         next(ex);
      })

  }, dkimAction(function(req,res,next){

      var dkimExisted = req.dkim && req.dkim.dkim_key_active;

      req.result = {dkimRemoved: dkimExisted, dnsSuccess: false};

      if(
        (!req.dnsZone)
        ||
        (!dkimExisted)
        )
      {
        return req.sendResponse(req.result);
      } 

      // we still need to remove the relevant records

      var host = getHostPrefix(req.dkim.host_prefix, req.dnsZone);
      return req.relayerWithTokenFromOriginalRequest.getAsync("/tapi/domain/"+req.dnsZone.dnsZone)
         .then(function(rex){
            var re = rex.result;
            var ps = [];

            re.records.forEach(function(record){
               if(record.type != "TXT") return;
               if(record.host != host) return;
               if(record.text != req.dkim.raw_txt_value) return;

               // this record needs to be removed!
               ps.push(
                req.relayerWithTokenFromOriginalRequest.deleteAsync("/tapi/domain/"+req.dnsZone.dnsZone+"/"+record.hash, {limit:1})
               );
            })

            return Promise.all(ps);
         })
         .then(()=>{
            req.result.dnsSuccess = true;
         })
         .catch(ex=>{
            console.error("Unable to delete DNS records for DKIM", ex)
         })
         .then(()=>{
            return req.sendResponse(req.result);
         })

  }));



   // Note: this route was added to circumvent the security restriction
   // around the derived tokens; we validate here first, then send the real
   // delete request with a derived token towards the accountapi servers 
   router.route("/webhostingdomains/")
     .delete(function(req,res,next){
         var d;
         return vali.async(req.body.json,{
             wd_server: {presence: true, isString: {strictName: true}},
             wd_webhosting: {presence: true, isInteger: {lazy: true}},
             wd_domain_name: {presence: true, isHost: {returnCanonical:true}},
         })
         .then(ad=>{
            d = ad;
            // now we need to query the raw bindings to learn if it has indeed been removed
            return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+d.wd_server+"/webhosting/"+d.wd_webhosting+"/domains/raw") 
         })
         .then(re=>{
            var stillThere = re.result.indexOf(d.wd_domain_name);
            if(stillThere != -1) throw new MError("DOMAIN_HAS_NOT_BEEN_REMOVED_YET");

            return req.relayerWithTokenFromOriginalRequest.deleteAsync("/account/webhostingdomains/", d) 
         })
         .then(re=>{
            return req.sendResponse(re.result);
         })
     })


   router.route("/webhosting/")

     .delete(function(req,res,next){

        var emitter;
        var server;
        var d;
        return vali.async(req.body.json, {wh_id: {presence: true, isInteger: true}, wh_user_id: {presence: true, isString: {lazy:true}}, server:{presence: true, isString:true}})
          .then(ad=>{
              d = ad;
              server = d.server;
              delete req.body.json.server;
              emitter = app.commander.EventEmitter()
              return emitter.spawn()
          })
          .then(eh=>{
             req.sendTask(Promise.resolve(eh));

             emitter.send_stdout_ln("Dispatching delete request to the appropriate relayer server");

             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+server+"/wizard/webhosting/", req.body.json);
          })
          .then(dh=>{

            var taskUrl = app.getLocalRelayerUrlPrefix() + "/s/"+server+"/tasks/"+ dh.result.id;

            const request = require("RequestCommon");
            var source = request.get(taskUrl);
            return emitter.relayRequest(source);
          })
         .then(()=>{
            // note the order, removal of webstore domain bindings is only possible before revokal of the main webstore grant
            emitter.send_stdout_ln("Removing webstore domain bindings"); 

            var customPayload = {wd_server: server, wd_webhosting: d.wh_id};

            return new Promise(function(resolve,reject){
                req.noUriTransform = true;
                return app.Relay("ServersAccountapi", "/accountapi/webhostingdomains/by-wh", req, function(err){
                   delete req.noUriTransform;

                   if(err) return reject(err);
                   return resolve();
                }, customPayload);
            })

         })
         .then(()=>{
            emitter.send_stdout_ln("Revoking account grant to the webstore in accountapi");

            return new Promise(function(resolve,reject){
                req.noUriTransform = true;
                return app.Relay("ServersAccountapi", "/accountapi/account/"+d.wh_user_id+"/webhosting/"+server+"/"+d.wh_id, req, function(err){
                   delete req.noUriTransform;

                   if(err) return reject(err);
                   return resolve();
                }, {});
            })

         })
         .then(()=>{
            emitter.send_stdout_ln("Webstore along with domain bindings removed successfully.")
            emitter.close()
         })
         .catch(ex=>{
            console.error("Error while doing webstore remove wizard failed", d, ex);
            emitter.send_stdout_ln("Error during the operation.");
            emitter.close(1);
         })

     })


  return router


}
