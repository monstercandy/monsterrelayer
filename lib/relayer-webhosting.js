/*
POST:
        "/s/s1/webhosting/12345": "ok",


GET:
                "/s/s1/webhosting/12345": {
                   wh_id: 12345,
                   wh_name: "some name",
                   wh_template: "WEB10000",
                },
                "/s/s1/webhosting/supported/php": [{"version":"5.3","deprecated": true}, {"version":"5.4","deprecated": true}, {"version":"5.5","deprecated": true}, {"version":"5.6"}, {"version":"7.0"}, ],


*/

module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  app.SetupRouterForPipeTasks(router, "ServersWebhosting", '/webhosting')

  const this_server_name = app.config.get("server_name");


  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_WEBHOSTING")) req.granted = true

     next()
  })

  router.post("/:webhosting/tallies", tallyLogic)
  router.post("/:webhosting/tallydetails", tallyLogic)
  router.post("/tallydetails", tallyLogic)
  router.post("/tallies", tallyLogic)

  Array("mail","web").forEach(cat=>{
    Array("jump","restore","checkpoints").forEach(method=>{
       var url = "/:webhosting/szar/"+method+"/"+cat
       router.post(url, GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
    })
  })

  router.post("/accesslog/by-webhosting/:webhosting/search", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant));
  router.post("/accesslog/by-webhosting/:webhosting/count", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant));

  router.post("/slowlog/by-webhosting/:webhosting/search", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant));
  router.post("/slowlog/by-webhosting/:webhosting/count", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant));

  router.get("/:webhosting/domains/raw",
    GrantHelper.grantChecker(GrantHelper.perWebhostingGrant),
    function(req,res,next){
       return req.sendPromResultAsIs(app.getWebhostingDomainsRaw(req));
    }
  )



  router.get("/:webhosting", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  router.get("/supported/php", GrantHelper.grantChecker(doGrant))

  router.post("/:webhosting", function(req,res,next){

        // this filtering should not affect tallies; pure crap code, sorry:(
      if(req.path.match(/\/(tallydetails|tallies)/)) return next()


       Object.keys(req.body.json).forEach(function(q){
          // via this API, regular users can change the following proprties only
          if(Array(
            "wh_name",
            "wh_php_version",

            "wh_installatron_server",
            "wh_ftp_accepted_from_latest_admin_ip_only",
            "wh_ftp_accepted_from_foreign_ip",
            "wh_notify_script_differences",
            "wh_notify_foreign_ftp_login_events"
          ).indexOf(q) <= -1)
             delete req.body.json[q]
       })
       next()

    },
    GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))


  router.get("/supported/python", function(req,res,next){
        req.granted = true
        return next()
    })

  router.route("/:webhosting/python")
    .get(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
    .put(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
    .delete(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))

  router.route("/:webhosting/python/:app")
    .get(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
    .delete(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))

  router.route("/:webhosting/python/:app/requirements")
    .post(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))

  router.route("/:webhosting/python/:app/restart")
    .post(GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))

  router.use(function(req,res,next){
  	 if(!req.granted)
  	 	 return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersWebhosting", "/webhosting"+req.path, req, next)
  })



  return router


  function doGrant(req) {
    req.granted = true
  }

  function tallyLogic(req,res,next){
     if(req.granted) return next()

     if(req.TokenUser.session.IsGranted("ADMIN_TALLY")) req.granted = true

     return next()
  }

}
