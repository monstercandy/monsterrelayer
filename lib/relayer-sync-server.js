
module.exports = function(app) {


  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()


  const timeoutOptions = {config:{timeout: app.config.get("sync_timeout")}}
  Array(
     {"name": "tapi", "grants": ["ADMIN_TAPI"]},
     {"name": "accountapi", "grants": ["ADMIN_ACCOUNTAPI"]}
  ).forEach(conf=>{

      router.post("/"+conf.name, function(req,res,next){

         if(!req.TokenUser.session.IsAnyGranted(allPerms(conf.grants))) return next(new MError("PERMISSION_DENIED"))

         var poolToRest
         return app.ServersLib.GetServers(conf.name)
           .then(servers=>{
              if(servers.length < 2) throw new MError("NOT_ENOUGH_SERVERS_CONFIGURED")

              var src = req.body.json.src
              if(!src) throw new MError("SRC_SERVER_NOT_SPECIFIED")

              if(req.body.json.dst) {
                 var nServers = [];
                 servers.forEach(s=>{
                    if((s.s_name == req.body.json.src)||(s.s_name == req.body.json.dst))
                      nServers.push(s);
                 })
                 servers = nServers;
              }

              poolToRest = app.ServersLib.GetRelayerPool(servers, req, [src])
              if((!poolToRest.skipped)||(!poolToRest.skipped[0])) throw new MError("SOURCE_SERVER_NOT_FOUND")

              var poolToSource = app.ServersLib.GetRelayerPool(poolToRest.skipped, req)

              return poolToSource.postAsync("/api/"+conf.name+"/export/tgz", req.body.json.payload || {}, timeoutOptions)
           })
           .then(getRes=>{
              return poolToRest.postAsync("/api/"+conf.name+"/import/tgz", getRes.result, timeoutOptions)
           })
           .then(postRes=>{
              return req.sendResponse(postRes.result);
           })

      })

  })




  return router

  function allPerms(additionalPermissions) {
     return ["SUPERUSER"].concat(additionalPermissions||[])
  }

}
