
module.exports = function(app, outerRouter, apiName, server, additionalPermissions, additionalRouter) {

  var apiPrefix = "/" + apiName

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const this_server_name = app.config.get("server_name");
  const MError = require('MonsterError')

  var allPerms = ["SUPERUSER"].concat(additionalPermissions||[])


  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){

     console.log("authorizing su request", req.path, req.TokenUser.session.s_grants, allPerms, req.params.server, this_server_name);

     var granted = false;
     if(!req.TokenUser.session.IsAnyGranted(allPerms)) 
     {
        if(req.TokenUser.session.IsGranted("SERVER_OWNER")) {
           var serverWanted = req.params.server || this_server_name;
           if(req.TokenUser.servers.indexOf(serverWanted) > -1)
              granted = true;
        }
        
     } 
     else
        granted = true;

     if(!granted)
       return next(new MError("PERMISSION_DENIED"))

     app.Relay(server, apiPrefix+req.path, req, next)
  })


  if(additionalRouter)
    outerRouter.use("/su"+apiPrefix, additionalRouter(apiName), router)
  else
    outerRouter.use("/su"+apiPrefix, router)

  return router
}
