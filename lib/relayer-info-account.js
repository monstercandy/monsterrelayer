/*
requests are granted for some specific info requests only

NOTE: dont forget to update the router-accountapi-misc.js, that is the receiver part of the chain on the accountapi servers
(and this one is the receiver on servers without accountapi)
*/

(function(){

  var lib = module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()


  const accountApiPrefix = "/accountapi"
  const accountInfoServerName = "ServersAccountapi"

  var vali = require("MonsterValidators").ValidateJs()


/*
// the following details should be stripped out from the response:

                        'u_name' => 'Monster Media',
                        'created_at' => '2010-03-23 17:55:00',
                        'updated_at' => '2010-03-23 17:55:00',
                        'u_primary_tel' => '+36-30-2679426',
                        'u_grant_access_for' => '0',
                        'u_primary_email_stripped' => 'info@monstermediahu',
                        'u_callin' => '',
                        'u_primary_email' => 'info@monstermedia.hu',
*/
  function accountLookup(callback){
    return function(req,res,next) {
      if(!req.granted) return next()

      var aPath = accountApiPrefix + req.path
      req.method = "GET"

      app.Relay(accountInfoServerName, aPath, req, function(err){
         if(err) return next(err);

         lib.filterMostUserInfoResponse(req.result);

         callback(err, req, res, next);
      })
    }
  }

  router.get("/account/:account_id", accountInfoBusinessLogic, accountLookup(function(err, req, res, next){
         lib.filterNameUserInfoResponse(req.result);

         next()
  }))

  router.post("/account/:account_id",
    emailInfoBusinessLogic,
    function(req,res,next){
      return vali.async(req.body.json, {emailCategory: {presence: true, isString: {lazy:true,strictName:true}}})
        .then(()=>{
           next()
        })
    },
    accountLookup(function(err, req, res, next){

      var oResult = req.result
      var aPath = accountApiPrefix + req.path + "/emails/"+req.body.json.emailCategory
      app.Relay(accountInfoServerName, aPath, req, function(err){
         if(err) return next(err);
         var eResult = req.result
         req.result = {account: oResult, email: eResult}

         return next()
      })

  }))

  router.post("/token", sessionInfoBusinessLogic, function(req,res,next){
      if(!req.granted) return next()

      var aPath = accountApiPrefix + "/authentication/token"
      app.Relay(accountInfoServerName, aPath, req, next)
  })


  router.get("/config/webhostingtemplates/dynamiclanguages",templateInfoBusinessLogic)
  router.get("/webhostingtemplate/:template",templateInfoBusinessLogic)
  router.get("/webhostingtemplates",templateInfoBusinessLogic)

  router.get("/servers", serverInfoBusinessLogic)



  router.use(function(req,res,next){

      if(req.result) return next()
      if(!req.granted) return next(new MError("PERMISSION_DENIED"))

      var aPath = accountApiPrefix + req.path

      app.Relay(accountInfoServerName, aPath, req, next)

  })


  return router


  function infoBusinessLogic(role, req,res,next) {
      if(req.TokenUser.session.IsSuperUser()) req.granted = true
      else
      if(req.TokenUser.session.IsGranted(role)) req.granted = true

      next()
  }


  function serverInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_SERVERS", req,res,next)
  }

  function accountInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_ACCOUNT", req,res,next)
  }
  function emailInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_EMAIL", req,res,next)
  }

  function sessionInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_SESSION", req,res,next)
  }

  function templateInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_WEBHOSTING_TEMPLATE", req,res,next)
  }
}


lib.filterMostUserInfoResponse= function(userInfoArr) {
         Array('created_at','updated_at','u_grant_access_for','u_primary_email_stripped','u_callin').forEach(n=>{
            delete userInfoArr[n]
         })
}
lib.filterNameUserInfoResponse= function(userInfoArr) {
         Array('u_primary_tel','u_primary_email').forEach(n=>{
            delete userInfoArr[n]
         })
}
})()

