module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()


  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersDbms", '/dbms')

  router.use(function(req,res,next){

     if((req.body)&&(req.body.json)) {
        delete req.body.json.force
        delete req.body.json.skip_backend
     }

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_DBMS")) req.granted = true

     next()
  })

  router.post("/slowlog/by-webhosting/:webhosting/search", relayIfGranted);
  router.post("/slowlog/by-webhosting/:webhosting/count", relayIfGranted);

  router.post("/databases/:webhosting/supported", relayIfGranted)
  router.route("/databases/:webhosting/:database/user/:dbuser")
    .get(relayIfGranted)
    .delete(relayIfGranted)

  Array("export","import","repair").forEach(c=>{
     router.post("/databases/:webhosting/:database/"+c, relayIfGranted)
  })

  router.route("/databases/:webhosting/:database/users")
    .get(relayIfGranted)
    .put(relayIfGranted)

  router.route("/databases/:webhosting/:database")
    .get(relayIfGranted)
    .delete(relayIfGranted)

  router.route("/databases/:webhosting")
    .get(relayIfGranted)
    .put(relayIfGranted, function(req,res,next){
        req.body.json.db_server = req.params.server
        req.body.json.db_user_id = req.TokenUser.account.u_id
        next()
    })


  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersDbms", "/dbms"+req.path, req, next)
  })

  function relayIfGranted(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
  }



  return router

}
