(function(){

    module.exports = function(app) {


       var this_server_name;
       var recaptchaParams
       if(app) {
          recaptchaParams = app.config.get("recaptcha");
          this_server_name = app.config.get("server_name");
       }

       if((app)&&(!app.captchaInited)) {

          app.captchaInited = true;

          app.captchaRecaptchaMode = (recaptchaParams && recaptchaParams.public && recaptchaParams.public.sitekey && recaptchaParams.secretkey);

          if(!app.captchaRecaptchaMode) {
            const memcache = require("SimpleMemcache");
            app.captchaStore = memcache(app.config.get("captcha_memcache"));
          } else {
             const recaptchaLib = require("lib-recaptcha.js");
             app.captchaRecaptcha = recaptchaLib(recaptchaParams);

          }

       }


      const MError = require("MonsterError");

      var re = {}


       function captchaMatches(a, b) {
          var case_sensitive = app.config.get("captcha_case_sensitive")
          return case_sensitive ? a == b : a.toLowerCase() == b.toLowerCase()

       }

       function captchaKey(req){
          return req.TokenUser ? "user-"+req.TokenUser.account.u_id : "ip-"+req.clientIp;
       }
       function getLocalCaptcha(req) {
            if(!app.captchaLib)
               app.captchaLib = require("Captcha")();

            var t = app.captchaLib.GetCaptcha( app.config.get("captcha_characters") )
            var key = captchaKey(req);
            app.captchaStore.set(key, t.text)
            return t;
       }

       re.configureCaptchaRoutes = function(router){
          router.get("/captcha", function(req,res,next){
            throw new MError("DEPRECATED");
          });
          router.get("/captcha/v2", function(req,res,next){
            if(app.captchaRecaptchaMode)
              return req.sendResponse({recaptcha: recaptchaParams.public});

            var t = getLocalCaptcha(req);
            req.sendResponse({html: t.captcha})
          });
       }

       function captchaEnforceClassic(req){
          var key = captchaKey(req);
          var stored = app.captchaStore.get(key);
          app.captchaStore.del(key)
          if((!req.body.json.captcha)||(!stored)||(!captchaMatches(stored, req.body.json.captcha)))
            throw new MError("CAPTCHA_MISMATCH")
          delete req.body.json.captcha
       }

       function captchaEnforceRecaptcha(req) {
          return app.captchaRecaptcha.VerifyAsync(req.body.json.captcha, req.clientIp);
       }


       re.captchaEnforceAsync = function(name, req) {
        return Promise.resolve()
          .then(()=>{

            if((name != "-")&&(!app.config.get(name)))
              return;

            if(app.captchaRecaptchaMode)
              return captchaEnforceRecaptcha(req);
            else
              return captchaEnforceClassic(req);

          })
       }

      re.grantChecker = function(extraChecker) {
        return function(req,res,next){
            if(req.granted) return next()

            if(!req.TokenUser.session.IsAnyGranted(["ACCOUNT_OWNER"])) return next()

            extraChecker(req,res,next)
            next()
        }

      }

      re.relayIfCertAttach=function(req,res,next){
         if(req.granted) return next()
         req.granted = req.TokenUser.session.IsGranted("CERT_ATTACH")
         next()
      }

      function perDomainGrantInt(req){
          for(var q of req.TokenUser.domains) {
             if(q.d_domain_canonical_name == req.params.domain){
              return true
             }
          }

          return false
      }

       function  perWebhostingGrantInt(req){

          return re.doesWebhostingBelongToUser(req, req.params.server, req.params.webhosting)

      }

      re.perWebhostingGrant = function(req,res,next){

          req.granted = perWebhostingGrantInt(req)
      }


      re.perDomainWebhostingGrant = function (req,res,next){

          req.granted = perWebhostingGrantInt(req) && perDomainGrantInt(req)
      }
      re.perDomainGrant = function  (req,res,next){

         req.granted = perDomainGrantInt(req)
      }

      re.doesWebhostingBelongToUser = function(req, server, webhosting_id) {
          if(server == "[this]") server = this_server_name;

          for(var q of req.TokenUser.webhostings) {

             if((q.w_server_name == server)&&(q.w_webhosting_id == webhosting_id)){
               return true
             }
          }

          return false
      }

      return re
    }

})();
