require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    var originalAuthToken = "test_token"
    var origMapi = app.GetMapiPool
    const MError = app.Error

    var accountApiResult = {"result":{"account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:["SUPERUSER"]}}}

    // we dont care about token auth in this test
     app.MonsterInfoSession = {
        GetInfo: function(token){
           assert.equal(token, originalAuthToken)
           return Promise.resolve(accountApiResult.result)
        }
     }


   var origServersLib = app.ServersLib

    describe('/s/.../test', function() {

       it("invalid servername", function(){

            app.ServersLib = {
               "GetServerByName": function(server_name){
                  assert.equal(server_name, "invalid")
                  return Promise.reject(new MError("SERVER_NOT_FOUND"))
               }
            }

          return mapi_token.postAsync("/s/invalid/tapi", {})
            .then((res)=>{
               throw new Error("should have been failed")
            })
            .catch(x=>{
               assert.equal(x.message, "SERVER_NOT_FOUND")
            })

       })

       it("valid local servername, should be served locally", function(){

            app.ServersLib = {
               "GetServerByName": function(server_name){
                  return Promise.reject(new MError("SHOULD_NOT_BE_CALLED"))
               }
            }

          return mapi_token.postAsync("/s/mocha/test", {})
            .then((res)=>{
               assert.equal(res.result, "POST {}")
            })

       })


       it("valid remote server, should be relayed", function(){

            app.ServersLib = {
               "GetServerByName": function(server_name){
                  assert.equal(server_name, "foo")
                  return Promise.resolve({"s_name": "foo"})
               },
               "GetRelayerPoolWithoutUs": function(servers){
                 return {
                    requestAsync: function(p){
                       return Promise.resolve({result:"foo"})
                    }
                 }
               }
            }

          return mapi_token.postAsync("/s/foo/test", {})
            .then((res)=>{
               assert.equal(res.result, "foo")
            })

       })


       it("valid remote server, tasks fetching, should be piped", function(done){

            var firstServer = {"some":"serverparams"}

            var appMapiCalled = 0
            var requestWasCalled = 0
            app.ServersLib = {
               "GetServerByName": function(server_name){
                  assert.equal(server_name, "foo")
                  return Promise.resolve({"s_name": "foo"})
               },
               "GetRelayerPoolWithoutUs": function(servers){
                 return {
                    GetRawServers: function(){
                       return [firstServer]
                    },
                    requestAsync: function(p){
                       return Promise.resolve({result:"foo"})
                    }
                 }
               }
            }
            var oAppMapi = app.Mapi
            app.Mapi = function(pipeConfig)  {
               // console.log(pipeConfig)
               assert.deepEqual(pipeConfig, { some: 'serverparams', pipe: true })
               appMapiCalled++

               return {request: function(method, uri){
                  // console.log("hey", method, uri)
                  assert.equal(method,"POST")
                  assert.equal(uri, "/api/s/[this]/dbms/tasks/something")
                  requestWasCalled++

                  // note: since pipeing is quite complex, we dont mock everything here, instead checking things only to this point

                  done()
               }}
            }
            app.Mapi.cloneServersAsPipe = oAppMapi.cloneServersAsPipe

          mapi_token.postAsync("/s/foo/dbms/tasks/something", {})

       })


    })

    describe('basic wizard tests', function() {

       it("testing whether wizard can relay requests internally", function(){

          return mapi_token.getAsync("/s/mocha/wizard/test")
            .then((res)=>{
               assert.deepEqual(res.result, 'POST {"hey":"world"}')
            })

       })


    })


   function getExpireDate(seconds) {
        var moment = require("MonsterMoment")
        return moment().add(seconds || 86400, "second").toISOString()
   }


})

