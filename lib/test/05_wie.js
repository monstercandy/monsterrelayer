require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    var origMapi = app.GetMapiPool
    const MError = app.Error
    var originalAuthToken = "test_token"
    var accountApiResult = {"result":{"account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:["SUPERUSER"]}}}

    var expectedWhIds = [10001, 10002];

    const oneBlock =
      { webhosting: { g1: 'ServersWebhosting' },
  cert: { g1: 'ServersCert' },
  dbms: { g1: 'ServersDbms' },
  docker: { g1: 'ServersDocker' },
  docrootapi: { g1: 'ServersDocrootapi' },
  email: { g1: 'ServersEmail' },
  fileman: { g1: 'ServersFileman' },
  ftp: { g1: 'ServersFtp' },
  git: { g1: 'ServersGit' }};

    const oneBlock2 =
      { webhosting: { g2: 'ServersWebhosting' },
  cert: { g2: 'ServersCert' },
  dbms: { g2: 'ServersDbms' },
  docker: { g2: 'ServersDocker' },
  docrootapi: { g2: 'ServersDocrootapi' },
  email: { g2: 'ServersEmail' },
  fileman: { g2: 'ServersFileman' },
  ftp: { g2: 'ServersFtp' },
  git: { g2: 'ServersGit' }};

    // we dont care about token auth in this test
     app.MonsterInfoSession = {
        GetInfo: function(token){
           assert.equal(token, originalAuthToken)
           return Promise.resolve(accountApiResult.result)
        }
     }

    var subsystems;

    describe('wie tests', function() {


       it("fetching wie for the complete", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch")
            .then((res)=>{
               assert.deepEqual(res.result, {})

            })

       })

       it("fetching wie for a particular webstore", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch/10001")
            .then((res)=>{
               throw new Error("Should nto be here");
            })
            .catch(ex=>{
               assert.propertyVal(ex, "message", "INTERNAL_ERROR"); // file does not exists
            })

       })

       it("fetching list of subsystems that have support for wie", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/config/subsystems")
            .then((res)=>{
               assert.ok(res.result.length > 0);
               assert.equal(res.result[0], "webhosting");
               subsystems = res.result;
            })

       })



       it("triggering a complete backup sequence", function(){

          var stats = {};

          var oGetMapiPool = app.GetMapiPool;
          app.GetMapiPool = function(name, poolOptions){
              if(!stats[name]) stats[name] = {GetMapiPool:0, getAsync:0};
              stats[name].GetMapiPool++;
              return {
                 getAsync: function(url) {
                    stats[name].getAsync++;
                    var base = {"g1":name};
                    var re = {};
                    expectedWhIds.forEach(wh_id=>{
                       re[wh_id] = base;
                    })
                    return Promise.resolve({result: re});
                 },
                 SetupFieldsByRequest: function(){
                   // we dont give a shit on this
                 },
              }
          }

          return mapi_token.postAsync("/s/mocha/su/wie/backup", {})
            .then((res)=>{
               assert.equal(res.result, "ok")
               assert.equal(Object.keys(stats).length, subsystems.length);

               app.GetMapiPool = oGetMapiPool;
            })

       })


       it("fetching wie for the complete webstore", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch")
            .then((res)=>{
               assert.deepEqual(res.result, { '10001': oneBlock , '10002':oneBlock })
            })

       })

       it("fetching wie for a particular webstore", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch/10001")
            .then((res)=>{
               // console.log(res.result)
               assert.deepEqual(res.result, oneBlock)
            })

       })


       it("triggering backup sequence for a particular store", function(){

          var stats = {};

          var wh_id = expectedWhIds[0];

          var oGetMapiPool = app.GetMapiPool;
          app.GetMapiPool = function(name, poolOptions){
              if(!stats[name]) stats[name] = {GetMapiPool:0, getAsync:0};
              stats[name].GetMapiPool++;
              return {
                 getAsync: function(url) {
                    assert.ok(url.endsWith("/"+wh_id));
                    stats[name].getAsync++;
                    var base = {"g2":name};
                    return Promise.resolve({result: base});
                 },
                 SetupFieldsByRequest: function(){
                   // we dont give a shit on this
                 },
              }
          }

          return mapi_token.postAsync("/s/mocha/su/wie/backup/"+wh_id, {})
            .then((res)=>{
               assert.equal(res.result, "ok")
               assert.equal(Object.keys(stats).length, subsystems.length);

               app.GetMapiPool = oGetMapiPool;
            })

       })


       it("fetching wie for the complete webstore", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch")
            .then((res)=>{
               assert.deepEqual(res.result, { '10001': oneBlock2 , '10002':oneBlock })

            })

       })


       it("doing restore for the complete system", function(){

          var stats = {};

          var oGetMapiPool = app.GetMapiPool;
          app.GetMapiPool = function(name, poolOptions){
              if(!stats[name]) stats[name] = {GetMapiPool:0, getAsync:0};
              stats[name].GetMapiPool++;
              return {
                 postAsync: function(url, payload) {
                    stats[name].postAsync++;
                    if(name == "ServersWebhosting")
                        assert.deepEqual(payload, {
                          '10001': { g2: 'ServersWebhosting' },
                          '10002': { g1: 'ServersWebhosting' }
                        });
                    return Promise.resolve({result: "ok"});
                 },
                 SetupFieldsByRequest: function(){
                   // we dont give a shit on this
                 },
              }
          }

          return mapi_token.postAsync("/s/mocha/su/wie/restore/", {})
            .then((res)=>{
               assert.equal(res.result, "ok")
               assert.equal(Object.keys(stats).length, subsystems.length);

               app.GetMapiPool = oGetMapiPool;

            })

       })

       it("doing restore for a particular store (all subsystems)", function(){

          var stats = {};

          var wh_id = expectedWhIds[0];

          var oGetMapiPool = app.GetMapiPool;
          app.GetMapiPool = function(name, poolOptions){
              if(!stats[name]) stats[name] = {GetMapiPool:0, getAsync:0};
              stats[name].GetMapiPool++;
              return {
                 postAsync: function(url, payload) {
                    assert.ok(url.endsWith("/"+wh_id));
                    if(name == "ServersWebhosting")
                       assert.deepEqual(payload, { g2: 'ServersWebhosting' })
                    //console.log("foooo", wh_id, name, payload)
                    stats[name].postAsync++;
                    return Promise.resolve({result: "ok"});
                 },
                 SetupFieldsByRequest: function(){
                   // we dont give a shit on this
                 },
              }
          }

          return mapi_token.postAsync("/s/mocha/su/wie/restore/"+wh_id, {})
            .then((res)=>{
               assert.equal(res.result, "ok")
               assert.equal(Object.keys(stats).length, subsystems.length);

               app.GetMapiPool = oGetMapiPool;
            })

       })


       it("doing restore for a particular store (cert and ftp subsystems only)", function(){

          var stats = {};

          var wh_id = expectedWhIds[1];

          var oGetMapiPool = app.GetMapiPool;
          app.GetMapiPool = function(name, poolOptions){
              if(!stats[name]) stats[name] = {GetMapiPool:0, getAsync:0};
              stats[name].GetMapiPool++;
              return {
                 postAsync: function(url, payload) {
                    assert.ok(url.endsWith("/"+wh_id));

                    if(name == "ServersFtp")
                      assert.deepEqual(payload, { g1: 'ServersFtp' })
                    stats[name].postAsync++;
                    return Promise.resolve({result: "ok"});
                 },
                 SetupFieldsByRequest: function(){
                   // we dont give a shit on this
                 },
              }
          }

          return mapi_token.postAsync("/s/mocha/su/wie/restore/"+wh_id, {subsystems: ["cert", "ftp"]})
            .then((res)=>{
               assert.equal(res.result, "ok")
               assert.equal(Object.keys(stats).length, 2);

               app.GetMapiPool = oGetMapiPool;
            })

       })

    })

  describe("feeding", function(){

      const newContent = {"webhosting":{"new":"content"}};
      var wh_id = expectedWhIds[0];

       var newFullContent = {};
       newFullContent[wh_id] = newContent;


       it("feeding the wie archive with new content", function(){

          return mapi_token.postAsync("/s/mocha/su/wie/feed/"+wh_id, newContent)
            .then((res)=>{
               assert.equal(res.result, "ok")

            })

       })

       it("fetching wie for a particular webstore", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch/"+wh_id)
            .then((res)=>{
               // console.log(res.result)
               assert.deepEqual(res.result, newContent)
            })

       })


       it("replacing the complete wie archive with new content", function(){

          return mapi_token.postAsync("/s/mocha/su/wie/feed/", newFullContent)
            .then((res)=>{
               assert.equal(res.result, "ok")
            })

       })



       it("fetching the complete wie", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/fetch/")
            .then((res)=>{
               // console.log(res.result)

               assert.deepEqual(res.result, newFullContent)
            })

       })

       it("fetching the list of webhosting ids covered by wie stores", function(){

          return mapi_token.getAsync("/s/mocha/su/wie/webstores/")
            .then((res)=>{
               // console.log(res.result)

               assert.deepEqual(res.result, [ '10001' ]);
            })

       })
  })


   function getExpireDate(seconds) {
        var moment = require("MonsterMoment")
        return moment().add(seconds || 86400, "second").toISOString()
   }



})

