require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    var originalAuthToken = "test_token"
    var origMapi = app.GetMapiPool
    const MError = app.Error
    var accountApiResult = {"result":{"account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:[]}}}
    var expiredAccountApiResult = {"result":{"account": {"u_id": 10}, "session": {"s_expires": getExpireDate(-60), s_grants:[]}}}
    var logoutResult = {"result":{"deleted": 1}}

    var accountServer = {"host":"127.0.0.1","port": 8000, "api_secret":"1234", "api_prefix": "/mpx", name: "ServersAccountapi"}
    var tapiServer    = {"host":"127.0.0.1","port": 8001, "api_secret":"12345", name: "ServersTapi"}


    // with the following ServersLib configuration everything will be served locally
    // this unit test aims testing some code related to entitlements
    app.ServersLib = {
       GetMyServerFromServersList: function(servers){
          return servers[0];
       },
       GetServers: function(){
          return Promise.resolve([{s_name: app.config.get("server_name")}])
       },
       GetTokenlessRelayerPoolWithoutUs: function(){
          return  app.ServersLib.GetRelayerPoolWithoutUs(); // we need the same logic in this group of unit tests
       },
       GetRelayerPoolWithoutUs: function(){
          var re = []
          re.doCallNext = true
          return re
       }
    }


    describe('configure servers', function() {

            it('trying to add a service without proper configuration parameters needed to be able to determine the api lib', function() {

                 return assert.isFulfilled(
                     mapi_token.internal.putAsync( "/service/accountapi", {"host":"127.0.0.1","port": 8000}).then(()=>{
                         throw new Error("should have been an exception already")
                     }).catch((err)=>{
                         assert.equal(err.message, "API_SECRET_NOT_SPECIFIED")
                     })
                 )

            })


           it('add accountapi service', function() {

                 return assert.isFulfilled(
                     mapi_token.internal.putAsync( "/service/accountapi", accountServer).then((res)=>{
                        assert.equal(res.result, "ok")
                     })
                 )

            })


           it('add tapi service', function() {

                 return assert.isFulfilled(
                     mapi_token.internal.putAsync( "/service/tapi", tapiServer).then((res)=>{
                        assert.equal(res.result, "ok")
                     })
                 )

            })
    })

    describe('relaying to accountapi without token/signatures', function() {
       it("checking login", function(){

            var userPass = {"username": "user", "password": "pass"}

            app.GetMapiPool = function(server, config){
                console.log("getting mapilib with", server)
                assert.equal(server, accountServer.name)
                assert.equal(app[server].FirstServer().api_prefix, "/mpx")
                var re = getMockedMapi()
                re.postAsync = function(uri, payload) {
                    assert.equal(uri, "/accountapi/authentication/credential") // the /mpx prefix should come from via api_prefix setting of the ServersAccountapi
                    assert.deepEqual(payload, userPass);

                    return Promise.resolve(accountApiResult)
                }
                return re
             }

          mapi_token.SetAuthToken("")
          return mapi_token.postAsync("/pub/account/authentication", userPass)
            .then((res)=>{
                 // console.log("login succeeded:", res)
                 assert.deepEqual(res.result, accountApiResult.result)
            })
       })
    })


    describe('captcha tests', function() {
        var origCaptcha

        function downloadCaptcha() {
               it("downloading captcha", function(done){
                  var was_called = 0
                  if(!origCaptcha) {
                      origCaptcha = app.captchaLib
                      app.captchaLib = {
                          GetCaptcha: function(c){
                             was_called++
                             return {text:"Xxxxxx", captcha: "graphical representation", type: "image/png"};
                          }

                      }
                  }


                  mapi_token.getAsync("/pub/captcha/v2")
                  .then(re=>{
                      //console.log(re.result);
                      assert.ok(re.result.html);
                      done();
                  })
                  .catch(done);

               })
       }

               it("old captcha route should be deprecated", function(done){

                  mapi_token.getAsync("/pub/captcha")
                       .then(()=>{assert.fail("should not happen")})
                       .catch((ex)=>{
                         assert.equal(ex.message, "DEPRECATED")
                         done();
                       })
                       .catch(done);

               })


       downloadCaptcha()

       it("incorrect captcha", function(){

          return mapi_token.postAsync("/pub/captcha", {"captcha":"incorrect"})
            .then(()=>{
                throw new MError("should not be here")
            })
            .catch((ex)=>{
                assert.equal(ex.message, "CAPTCHA_MISMATCH")
            })
       })

       downloadCaptcha()

       it("correct captcha with lowercase spelling (which should be accepted as default)", function(){

          return mapi_token.postAsync("/pub/captcha", {"captcha":"xxxxxx"})
            .then((res)=>{
                assert.equal(res.result, "ok")
                app.pub.captcha = origCaptcha
            })
       })
    })

    describe('relaying to business logic', function() {
        "use strict"



            it('testing invalid token', function() {

                 var invalid_token = "invalid"
                 mapi_token.SetAuthToken(invalid_token)


                 var mapiPoolWasCalled = false
                 app.MonsterInfoSession = {
                    GetInfo: function(token){
                       assert.equal(token, invalid_token)
                       mapiPoolWasCalled = true
                       return Promise.reject(new MError("AUTH_FAILED"))
                    }
                 }


                 return assert.isFulfilled(
                     mapi_token.getAsync( "/tapi/domain/test.hu")
                       .then(()=>{assert.fail("should not happen")})
                       .catch((ex)=>{
                         assert.equal(ex.message, "INVALID_TOKEN_OR_SESSION_EXPIRED")
                         assert.equal(mapiPoolWasCalled, true)
                       })
                 )

            })




            it('checking if accountapi authentication takes place', function() {

                 mapi_token.SetAuthToken(originalAuthToken) // change back


                 app.MonsterInfoSession = {
                    GetInfo: function(token){
                       assert.equal(token, originalAuthToken)
                       return Promise.resolve(accountApiResult.result)
                    }
                 }


                 return assert.isFulfilled(
                     mapi_token.getAsync( "/tapi/domain/test.hu")
                       .then(()=>{assert.fail("should not happen")})
                       .catch((ex)=>{
                         assert.equal(ex.message, "PERMISSION_DENIED")
                       })
                 )

            })





            it('checking expired token', function() {

                 mapi_token.SetAuthToken(originalAuthToken) // change back


                 app.MonsterInfoSession = {
                    GetInfo: function(token){
                       assert.equal(token, originalAuthToken)
                       return Promise.resolve(expiredAccountApiResult.result)
                    }
                 }


                 return assert.isFulfilled(
                     mapi_token.getAsync( "/tapi/domain/test.hu")
                       .then(()=>{assert.fail("should not happen")})
                       .catch((ex)=>{
                         assert.equal(ex.message, "SESSION_EXPIRED")
                       })
                 )

            })


            var tests = [
              { "name": "superuser tests", "s_grants": [ "SUPERUSER" ], "server": tapiServer,"domains": [],
                "calls": [
                    {u:"/tapi/build"},
                    {u:"/tapi/soa-contact"},
                    {u:"/tapi/export/tgz"},
                    {u:"/tapi/domain/test.hu"}
                 ]
              },
              { "name": "admin_tapi tests", "s_grants": [ "ADMIN_TAPI" ], "server": tapiServer,"domains": [],
                "calls": [
                    {u:"/tapi/build"},
                    {u:"/tapi/soa-contact"},
                    {u:"/tapi/export/tgz"},
                    {u:"/tapi/domain/test.hu"}
                 ]
              },
              { "name": "dns admin tests", "s_grants": [ "ACCOUNT_DNS" ], "server": tapiServer,"domains": [{"d_domain_canonical_name":"test.hu"}],
                 "unsuccessful": 3, // last three of them should be rejected with permission denied:
                 "calls": [
                    {u:"/tapi/domain/test.hu"},
                    {u:"/tapi/build", reject: true},
                    {u:"/tapi/soa-contact", reject: true},
                    {u:"/tapi/export/tgz", reject: true},
                 ]
              },
              { "name": "accountapi", "s_grants": [ "SUPERUSER" ], "domains": [], "server": accountServer,
                "calls": [
                    {u:"/su/accountapi/accounts", expectedUri: "/accountapi/accounts"},
                    {u:"/su/accountapi/account/10/", expectedUri: "/accountapi/account/10/",returnResult:{"u_id":10,"u_name":"name","u_comment":"comment"}},
                    {u:"/su/accountapi/account/10/", expectedUri: "/accountapi/account/10/",method:"POST",sendPayload:{"u_name":"newname","u_comment":"new comment"}},
                 ]
              },
              { "name": "accountapi", "s_grants": [ "ACCOUNT_OWNER" ], "domains": [], "server": accountServer,
                "calls": [
                    {u:"/su/accountapi/accounts", reject: true},
                 ]
              },
              { "name": "account", "s_grants": [ "ACCOUNT_OWNER" ], "domains": [], "server": accountServer,
                "calls": [
                    {u:"/account/accounts", reject: true},
                    {u:"/account/",expectedUri:"/accountapi/account/10/",returnResult:{"u_id":10,"u_name":"name","u_comment":"comment"},expectedResult:{"u_id":10,"u_name":"name"}},

                    // this one validates whether only whitelisted properties are relayed to accountapi during account modification
                    {u:"/account/",expectedUri:"/accountapi/account/10/",method:"POST",sendPayload:{"u_name":"newname","u_comment":"new comment"},expectedPayload:{ u_name: 'newname',
  u_email_login_ok_subsystems: undefined,
  u_email_login_fail: undefined,
  u_email_login_ok: undefined,
  u_language: undefined,
  u_callin: undefined,
  u_sms_notification: undefined }},
                 ]
              },
              /*
              */
            ]


            for (let test of tests) {

                for(let call of test.calls) {

                    if(!call.method) call.method = "GET"

                    it(test.name+" "+call.method+" "+call.u+" "+test.s_grants, function() {



                         mapi_token.SetAuthToken(originalAuthToken) // change back

                         accountApiResult.result.session.s_grants = test.s_grants
                         accountApiResult.result.domains = test.domains



                         app.GetMapiPool = function(server, config){

                            console.log("getting mapilib with", server)

                            assert.equal(server, test.server.name)

                            var re = getMockedMapi()

                            re.request = function(requestOptions, readyCallback) {

                                assert.propertyVal(requestOptions, "method", call.method.toUpperCase())
                                assert.propertyVal(requestOptions, "uri",  call.expectedUri || call.u)

                                if(call.sendPayload){
                                   // console.log("!!!!!!!!!!!",call.expectedPayload, call.sendPayload, payload)
                                   assert.property(requestOptions, "payload")
                                   assert.deepEqual(requestOptions.payload,  call.expectedPayload || call.sendPayload)
                                }

                                readyCallback(null, call.returnResult || "some_result")//
                            }
                            return re
                         }

                         app.MonsterInfoSession = {
                            GetInfo: function(token){
                               assert.equal(token, originalAuthToken)
                               return Promise.resolve(accountApiResult.result)
                            }
                         }

                         var prom = mapi_token[call.method.toLowerCase()+"Async"](call.u, call.sendPayload)


                         if(!call.reject) {
                                // console.log("expecting fullfilled!")
                                 return assert.isFulfilled(
                                     prom
                                       .then((res)=>{
                                          assert.deepEqual(res.result, call.expectedResult || call.returnResult || "some_result")
                                       })

                                 )
                           }


                         // it should be rejected with permission denied
                         return assert.isFulfilled(
                             prom
                               .then((res)=>{
                                  assert.fail("should not be here")
                               })
                               .catch(ex=>{
                                console.log("ex", ex)
                                  assert.equal(ex.message, "PERMISSION_DENIED")
                               })

                         )


                    })

                }



            }





            it("switch_user test (permission denied)", function() {

                 mapi_token.SetAuthToken(originalAuthToken) // change back

                 accountApiResult.result.session.s_grants = ["ACCOUNT_OWNER"]
                 accountApiResult.result.domains = []


                 app.GetMapiPool = function(server, config){

                    console.log("getting mapilib with", server, accountServer)

                    assert.equal(server, accountServer.name)

                    var re = getMockedMapi()

                    re.getAsync = function(uri) {
                        assert.equal(uri, "/accountapi/account/10/account_grants/for")
                        var x = {
                            result: [{u_id: "userid"}]
                        }
                        return Promise.resolve(x)
                    }

                    re.request = function(method, uri, payload, readyCallback) {

                        assert.fail("should not be here")
                    }

                    return re
                 }



                 return assert.isFulfilled(
                     mapi_token.postAsync("/account/switch_user", {"account_id": "account_id_to_switch_to"})
                       .then((res)=>{
                          assert.fail("should not be here")
                       })
                       .catch(ex=>{
                         assert.propertyVal(ex, "message", "PERMISSION_DENIED")
                       })
                 )

            })



            it("switch_user test (access granted)", function() {

                 mapi_token.SetAuthToken(originalAuthToken) // change back

                 accountApiResult.result.session.s_grants = ["ACCOUNT_OWNER"]
                 accountApiResult.result.domains = []


                 app.GetMapiPool = function(server, config){

                    console.log("getting mapilib with", server, accountServer)

                    assert.equal(server, accountServer.name)

                    var re = getMockedMapi()

                    re.getAsync = function(uri) {
                        assert.equal(uri, "/accountapi/account/10/account_grants/for")
                        var x = {
                            result: [{u_id: "account_id_to_switch_to"}]
                        }

                        return Promise.resolve(x)
                    }

                    re.request = function(requestOptions, readyCallback) {

                        assert.propertyVal(requestOptions, "method", "POST")
                        assert.propertyVal(requestOptions, "uri",  "/accountapi/authentication/switch_user")
                        assert.property(requestOptions, "payload")
                        assert.deepEqual(requestOptions.payload,  {account_id: "account_id_to_switch_to", on_behalf: 10}) // we send on_behalf as well now automatically

                        readyCallback(null, {"token":"new_token_id"})
                    }

                    return re
                 }



                 return assert.isFulfilled(
                     mapi_token.postAsync("/account/switch_user", {"account_id": "account_id_to_switch_to"})
                       .then((res)=>{
                          assert.propertyVal(res.result, "token", "new_token_id")
                       })

                 )

            })






            it("iptables firewall request should be rejected if private ip", function() {

                 accountApiResult.result.session.s_grants = ["ACCOUNT_OWNER"]
                 accountApiResult.result.domains = []
                 accountApiResult.result.webhostings = [{w_server_name:"mocha", w_webhosting_id:"1234"}]

                 return mapi_token.putAsync("/s/mocha/iptables/whextra/1234/firewallrules", {"fw_dst_ip":"127.0.0.1"})
                     .then((res)=>{
                         assert.fail(new Error("should have failed"))
                     })
                     .catch(ex=>{
                        assert.propertyVal(ex, "message", "VALIDATION_ERROR")
                     })


            })



            it("iptables firewall request should be fine otherwise", function() {

                 accountApiResult.result.session.s_grants = ["ACCOUNT_OWNER"]
                 accountApiResult.result.domains = []
                 accountApiResult.result.webhostings = [{w_server_name:"mocha", w_webhosting_id:"1234"}]

                 app.GetMapiPool = function(server, config){

                    console.log("getting mapilib with", server)

                    assert.equal(server, "ServersIptables")

                    var re = getMockedMapi()

                    re.request = function(requestOptions, readyCallback) {

                        assert.propertyVal(requestOptions, "method", "PUT")
                        assert.propertyVal(requestOptions, "uri",  "/iptables/whextra/1234/firewallrules")

                        readyCallback(null, {"great":"victory"})
                    }

                    return re
                 }


                 return mapi_token.putAsync("/s/mocha/iptables/whextra/1234/firewallrules", {"fw_dst_ip":"123.0.0.1"})
                     .then((res)=>{
                         assert.propertyVal(res.result, "great", "victory")
                     })


            })




            it('logout', function() {

                 mapi_token.SetAuthToken(originalAuthToken) // change back

                 var mapiPoolWasCalled = false
                 var delWasCalled = false
                 app.MonsterInfoSession = {
                    Delete: function(token){
                       assert.equal(token, originalAuthToken)
                       delWasCalled = true
                    },
                    GetInfo: function(token){
                       assert.equal(token, originalAuthToken)
                       mapiPoolWasCalled = true
                       return Promise.resolve(accountApiResult.result)
                    }
                 }


                 app.GetMapiPool = function(server, config){
                    console.log("getting mapilib with", server)
                    var re = getMockedMapi()
                    re.deleteAsync = function(uri, payload) {
                        assert.equal(uri, "/accountapi/token/"+originalAuthToken)

                        return Promise.resolve(logoutResult)
                    }
                    return re
                 }


                 return mapi_token.postAsync( "/logout")
                       .then((re)=>{
                         assert.equal(delWasCalled, true)
                         assert.equal(re.result, "ok")
                       })

            })



    })

    describe("ping", function(){

       it("the server should respond to /api/ping (not the same as the global one in MonsterExpress for the internal requests)", function(){

          return mapi_token.getAsync("/ping")
            .then((res)=>{
                assert.equal(res.result, "pong")
            })
       })

    })



   function getMockedMapi(){
        var re = {}
        re.getApiType = function(){
          return "internal"
        }
        re.SetupFieldsByRequest = function(req){
            re.SetXForwardedFor(req.ip)
        }
        re.SetXForwardedFor = function(ip) {
            assert.equal(ip, "::ffff:127.0.0.1")
        }
        return re
   }


   function getExpireDate(seconds) {
        var moment = require("MonsterMoment")
        return moment().add(seconds || 86400, "second").toISOString()
   }

})

