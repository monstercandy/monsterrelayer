require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    const MError = app.Error

    var service_primary_domain = app.config.get("service_primary_domain");
    var server_full_hostname = app.config.get("server_name")+"."+service_primary_domain;
    var port = app.config.get("listen_port");

    describe("ping", function(){

       corsTest(service_primary_domain);
       corsTest(server_full_hostname);

       corsTestNegative("foobar.hu");
       corsTestNegative("foobar."+service_primary_domain);
       corsTestNegative(service_primary_domain+".foobar");


       function corsTestNegative(origin){
           it("CORS preflight request with a invalid Origin header shall be refused"+origin, function(done){

              return mapi_token.requestLib({method: "OPTIONS", url: "http://127.0.0.1:"+port+"/pub/foobar", headers: {"Origin": origin}},
                function (error, response, body) {
                    // console.log(error, body, response)
                    assert.equal(response.statusCode, 500);
                    done();
                })

           })
      }



       function corsTest(origin){
           it("sending a CORS preflight request with a correct Origin header: "+origin, function(done){

              return mapi_token.requestLib({method: "OPTIONS", url: "http://127.0.0.1:"+port+"/pub/foobar", headers: {"Origin": origin}},
                function (error, response, body) {
                    if(error) return done(error);

                    assert.equal(response.headers['access-control-allow-origin'], origin)
                    assert.equal(body, "ok");

                    done();
                })

           })

       }


    })



})

