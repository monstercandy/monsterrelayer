require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    var originalAuthToken = "test_token"
    var origMapi = app.GetMapiPool
    const MError = app.Error

    var accountApiResult = {"result":{"derived_token": false, "account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:[]}}}

    // we dont care about token auth in most of these tests
    setupSessionLookupResult()

   var origServersLib = app.ServersLib

    describe('without monsterdirect', function() {

       it("post, we are the only server, can serve locally", function(){

            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.length = 0
                  re.doCallNext = true
                  return re
               }
            }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 assert.deepEqual(res.result, 'POST {"k":"v"}')
                 // console.log("login succeeded:", res)
            })
       })

       it("post, there is one more server, can serve locally", function(){

            var requestAsyncWasCalled = false
            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.requestAsync = function(){
                      requestAsyncWasCalled = true
                      return Promise.resolve()
                  }
                  re.length = 1
                  re.doCallNext = true
                  return re
               }
            }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 assert.deepEqual(res.result, 'POST {"k":"v"}')
                 assert.equal(requestAsyncWasCalled, true)
            })
       })

       it("post, there is one more server, can NOT serve locally", function(){

            var requestAsyncWasCalled = false
            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.requestAsync = function(){
                      requestAsyncWasCalled = true
                      return Promise.resolve({result:"hello"})
                  }
                  re.length = 1
                  re.doCallNext = false
                  return re
               }
            }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 assert.deepEqual(res.result, 'hello') // result should come from the relayed call!
                 assert.equal(requestAsyncWasCalled, true)

            })
       })

       it("post, there is one more server (which drops an exception), can NOT serve locally", function(){

            var requestAsyncWasCalled = false
            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.requestAsync = function(){
                      requestAsyncWasCalled = true
                      return Promise.reject(new MError("some foobar"))
                  }
                  re.length = 1
                  re.doCallNext = false
                  return re
               }
            }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 throw new Error("should have been an expcetion already")
            })
            .catch(x=>{
                    assert.equal(requestAsyncWasCalled, true)
                    assert.equal(x.message, "some foobar")
            })
       })


       it("post, testing GetRelayerPoolWithoutUs should reject in derived mode", function(){

           try{
            origServersLib.GetRelayerPoolWithoutUs([
                {s_name: "foo1"}, 
                {s_name: "foo2"}
               ], {TokenUser:{derived_mode: true, derived_server_tokens: {}}})
              throw new MError("should have failed")
           } catch(ex) {
              assert.propertyVal(ex, "message", "CANT_RELAY_WITH_DERIVED_TOKEN")
           }
       })

       it("post, testing GetRelayerPoolWithoutUs, missing server in derived_server_tokens", function(){

           try{
            origServersLib.GetRelayerPoolWithoutUs([
                {s_name: "foo1"}, 
                {s_name: "foo2"}
               ], {TokenUser:{derived_mode: false, derived_server_tokens: {}}})
              throw new MError("should have failed")
           } catch(ex) {
              assert.propertyVal(ex, "message", "CANT_RELAY_TO")
           }
       })

       it("post, testing GetRelayerPoolWithoutUs, without us in the list", function(){

           var x = origServersLib.GetRelayerPoolWithoutUs([
                {s_name: "foo1"}, 
                {s_name: "foo2"}
               ], {TokenUser:{derived_mode: false, derived_server_tokens: {foo1: "dervived:foo1", foo2: "derived:foo2"}}})

           assert.propertyVal(x, "length", 2)
           assert.propertyVal(x, "doCallNext", false)
       })

       it("post, testing GetRelayerPoolWithoutUs, with us in the list", function(){

           var x = origServersLib.GetRelayerPoolWithoutUs([
                {s_name: "foo1"}, 
                {s_name: "mocha"}
               ], {TokenUser:{derived_mode: false, derived_server_tokens: {foo1: "derived:foo2"}}})

           assert.propertyVal(x, "length", 1)
           assert.propertyVal(x, "doCallNext", true)
       })

       it("post, there are NO servers, can NOT serve locally", function(){

            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve([]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.length = 0
                  re.doCallNext = false
                  return re
               }
            }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 throw new Error("should have failed")
            })          
            .catch(x=>{
               assert.equal(x.message, "INTERNAL_ERROR")
            })
       })




       it("get, there is one more server, can serve locally", function(){

            var requestAsyncWasCalled = false
            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.requestAsync = function(){
                      requestAsyncWasCalled = true
                      return Promise.resolve()
                  }
                  re.length = 1
                  re.doCallNext = true
                  return re
               }
            }

          return mapi_token.getAsync("/test")
            .then((res)=>{
                 assert.deepEqual(res.result, 'GET {}')
                 assert.equal(requestAsyncWasCalled, false)
            })
       })

       it("get, there is one more server, can NOT serve locally", function(){

            var requestAsyncWasCalled = false
            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["server1","server2"]) },
               "GetRelayerPoolWithoutUs": function(servers) {
                  var re = {}
                  re.requestAsync = function(){
                      requestAsyncWasCalled = true
                      return Promise.resolve({result:"hello"})
                  }
                  re.length = 1
                  re.doCallNext = false
                  return re
               }
            }

          return mapi_token.getAsync("/test")
            .then((res)=>{
                 assert.deepEqual(res.result, 'hello') // result should come from the relayed call!
                 assert.equal(requestAsyncWasCalled, true)

            })
       })


    })


    describe('direct mode', function() {
       // these are the situations when the request is already relayed and no additional relaying is expected


       it("post, incorrect secret should be a permission denied", function(){

           var invalid_token = "abcd:1234"
           mapi_token.SetAuthToken(invalid_token) 
           app.MonsterInfoSession = {
              GetInfo: function(token){
                 assert.equal(token, invalid_token)
                 return Promise.reject(new MError("AUTH_FAILED"))
              }
           }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                throw new Error("should have failed")
            })
            .catch((x)=>{
                assert.propertyVal(x, "message","INVALID_TOKEN_OR_SESSION_EXPIRED")
            })
       })


       it("post, secret matching should serve locally", function(){

           var derived_token = "correctabcd:correct1234"
           mapi_token.SetAuthToken(derived_token) 
           app.MonsterInfoSession = {
              GetInfo: function(token){
                 assert.equal(token, derived_token)
                 return Promise.resolve({derived_mode: true, "account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:[]}})
              }
           }

          return mapi_token.postAsync("/test", {"k":"v"})
            .then((res)=>{
                 assert.deepEqual(res.result, 'POST {"k":"v"}')
            })
       })


    })


    describe('servers query', function() {

       it("querying available servers (not enough credentials)", function(){

           mapi_token.SetAuthToken(originalAuthToken) 
            setupSessionLookupResult()

            return mapi_token.getAsync("/su/servers")
              .then((res)=>{
                 throw new Error("should have been an exception")
              })
              .catch(x=>{
                 assert.propertyVal(x, "message","PERMISSION_DENIED")
                 accountApiResult.result.session.s_grants.push("ADMIN_FOOBAR")
              })
       })

       Array("", "foobar").forEach(role=>{

           it("querying available servers (enough credentials) with role: "+role, function(){

                const serverList = [{"s_name":"server1"},{"s_name":"server2"}]

                app.ServersLib = {
                   "GetServers": function(erole){ 
                      assert.equal(erole || "", role)
                      return Promise.resolve(serverList) 
                    },
                }

                return mapi_token.getAsync("/su/servers/"+role)
                  .then((res)=>{
                     assert.deepEqual(res.result, serverList.map(q=>{return q.s_name}))
                  })
           })

       })

    })


   function getExpireDate(seconds) {
        var moment = require("MonsterMoment")
        return moment().add(seconds || 86400, "second").toISOString()    
   }

   function setupSessionLookupResult(){
     app.MonsterInfoSession = {
        GetInfo: function(token){
           assert.equal(token, originalAuthToken)
           return Promise.resolve(accountApiResult.result)
        }
     }

   }

})

