require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../relayer-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi_token, assert){

    var originalAuthToken = "test_token"
    var origMapi = app.GetMapiPool
    const MError = app.Error
    const tgzData = "some huge data"

    var accountApiResult = {"result":{"account": {"u_id": 10}, "session": {"s_expires": getExpireDate(), s_grants:["SUPERUSER"]}}}

    // we dont care about token auth in this test
     app.MonsterInfoSession = {
        GetInfo: function(token){
           assert.equal(token, originalAuthToken)
           return Promise.resolve(accountApiResult.result)
        }
     }


    describe('sync (server)', function() {

       it("tapi missing src", function(){

            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["src","dst"]) },
            }        

          return mapi_token.postAsync("/su/sync/tapi", {})
            .then((res)=>{
               throw new Error("should have been failed")
            })
            .catch(x=>{
               assert.equal(x.message, "SRC_SERVER_NOT_SPECIFIED")
            })

       })

       it("tapi src not found", function(){

            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["src","dst"]) },
               "GetRelayerPool": function(servers) {
                  var re = {}
                  re.skipped = []
                  return re
               }
            }        
            
          return mapi_token.postAsync("/su/sync/tapi", {src: "foobar"})
            .then((res)=>{
               throw new Error("should have been failed")
            })
            .catch(x=>{
               assert.equal(x.message, "SOURCE_SERVER_NOT_FOUND")
            })

       })


       it("tapi", function(){

            app.ServersLib = {
               "GetServers": function(role){ return Promise.resolve(["src","dst"]) },
               "GetRelayerPool": function(servers) {
                  var re = {}
                  if(servers.length == 2) {
                     re.skipped = ["src"]
                     re.postAsync = function(uri, payload){
                        assert.equal(uri, "/api/tapi/import/tgz")
                        assert.equal(payload, tgzData)
                        return Promise.resolve({result:{"extracted": 23}})
                     }
                  }
                  else {
                     re.postAsync = function(uri){
                        assert.equal(uri, "/api/tapi/export/tgz")
                        return Promise.resolve({result:tgzData})
                     }
                  }
                  return re
               }
            }

          return mapi_token.postAsync("/su/sync/tapi", {src: "src"})
            .then((res)=>{
                 assert.deepEqual(res.result, {extracted: 23})
                 // console.log("login succeeded:", res)
            })
       })

    })


    describe('sync (internal)', function() {

       it("tapi", function(){

             var mapiCalls = 0
             app.Mapi.getMapiPool = function(serverRouter, config){
                // console.log("getting mapilib with", serverRouter)
                mapiCalls++
                if(mapiCalls == 1) {
                   return {
                      getAsync: function(uri){
                         assert.equal(uri, "/tapi/export/tgz")
                         return Promise.resolve({result:tgzData})
                      }
                   }
                }
                if(mapiCalls == 2) {
                   return {
                      postAsync: function(uri, payload){
                         assert.equal(uri, "/tapi/import/tgz")
                         assert.equal(payload, tgzData)
                         return Promise.resolve({result:{ extracted: 23 }})
                      }
                   }
                }
             }
            
            app.ServersTapi = {
               GetRawServers: function(){
                  return ["server1", "server2"]
               }
            }
            app.ServersLib = {}

          return mapi_token.postAsync("/s/mocha/su/sync/tapi", {})
            .then((res)=>{
                 assert.deepEqual(res.result, {extracted: 23})
                 // console.log("login succeeded:", res)
            })
       })

    })


   function getExpireDate(seconds) {
        var moment = require("MonsterMoment")
        return moment().add(seconds || 86400, "second").toISOString()    
   }


})

