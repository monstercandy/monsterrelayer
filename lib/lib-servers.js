module.exports = function(app){

    const moment = require("MonsterMoment")
    const MError = require('MonsterError')

	var cachedServersAll
	var cachedTimestamp

    const MonsterInfoLib = require("MonsterInfo")
    // app.ServersInfoAccountapi has already been configured in MonsterExpress
    app.MonsterInfoServers = MonsterInfoLib.servers({config: app.config, accountServerRouter: app.ServersInfoAccountapi})

	var re = {}


	re.GetAllServers = function(){
        return app.MonsterInfoServers.GetInfo()
	}

	re.GetServers = function(filterRole, throwIfEmpty, filterUser, filterServer){
       return re.GetAllServers()
         .then(servers=>{

         	if(!filterRole) return servers;

            var re = []
         	servers.forEach(server=>{
         		if(server.s_roles.indexOf(filterRole) > -1)
         			re.push(server)
         	})

         	return re;
         })
         .then(servers=>{

            if(!filterUser) return servers;

            var re = []
            servers.forEach(server=>{
                if(server.s_users.indexOf(filterUser) > -1)
                    re.push(server)
            })

            return re;
         })         
         .then(serversFiltered=>{
         	if((throwIfEmpty)&&(serversFiltered.length <= 0))
         		throw new MError("SERVER_NOT_FOUND")

         	return Promise.resolve(serversFiltered)
         })
	}
	re.GetServerByName = function(name){
       return re.GetAllServers()
         .then(servers=>{

            var re
         	servers.some(server=>{
         		if(server.s_name == name) {
         			re = server
         			return true
         		}
         	})

         	if(!re)
         		throw new MError("SERVER_NOT_FOUND")

         	return Promise.resolve(re)
         })
	}

    re.GetMyServer = function(){
    	return re.GetServerByName(app.config.get("server_name"))
    }
    re.GetMyServerFromServersList = function(servers){
        var this_server = app.config.get("server_name");
        for(var q of servers){
            if(q.s_name == this_server)
                return q;
        }
    }

    re.getFullHostname = function(server_name) {
        return server_name + "." + app.config.get("service_primary_domain")
    }

    function getServerFullHostname(server) {
        return re.getFullHostname(server.s_name)
    }

    function getMapiPoolFor(otherServers, poolOptions){
         var otherServersRouter = {
            GetRawServers: function(){return otherServers}
         }
         if(!poolOptions) poolOptions = {}
         if(!poolOptions.config) poolOptions.config = app.config

         var mc = app.Mapi.getMapiPool(otherServersRouter, poolOptions)

         return mc
    }

    re.GetServerRandomly = function(servers){
        var selectedServer = servers[Math.floor(Math.random()*servers.length)];
        return selectedServer
    }

    re.GetRelayerRandomly = function(servers, req, poolOptions) {

        var TokenUser = req.TokenUser

        var selectedServer = re.GetServerRandomly(servers)

        var auth_token = TokenUser.derived_server_tokens[selectedServer.s_name]
        if(!auth_token) {
            console.error("Was trying to select a relayer randomly (", selecetedServer.s_name,"), but no derived token was found");
            throw new MError("CANT_RELAY_TO", selectedServer.s_name);
        }


        var dest = [{
            server_name: selectedServer.s_name,
            host: getServerFullHostname(selectedServer),
            scheme: "https",
            auth_token: auth_token
        }]

        var mc = getMapiPoolFor(dest, poolOptions)
        mc.server_name = selectedServer.s_name
        return mc
    }

	re.GetRelayerPool = function(servers, req, skipList, poolOptions) {

         // the public api requsts have no TokenUser, we still need to support such requests
         var TokenUser = req.TokenUser || {derived_server_tokens:{}}

         // it is impossible to relay to other hosts, since we dont have the original primary token, only a derived one
         if(TokenUser.derived_mode)
            throw new MError("CANT_RELAY_WITH_DERIVED_TOKEN")

		 if(!skipList) skipList = []

         var otherServers = []
         var skipped = []
         servers.forEach(server=>{

         	if(skipList.indexOf(server.s_name) > -1) {
         		skipped.push(server)
         		return
         	}

         	var host = getServerFullHostname(server)

            var auth_token = TokenUser.derived_server_tokens[server.s_name]
            if((!auth_token)&&(req.isPublic)) auth_token = "-"; // this results in repeating the request simply without a token
            if(!auth_token) {
                console.error("Was trying to relay message to", server.s_name, req.originalUrl, "but no derived auth token was available");
                throw new MError("CANT_RELAY_TO", server.s_name);
            }

         	otherServers.push({
         		host: host,
         		scheme: "https",
         		auth_token: auth_token
         	})

         })


         var mc = getMapiPoolFor(otherServers, poolOptions)
	     mc.length = otherServers.length
		 mc.skipped = skipped
		 return mc
	}
	re.GetRelayerPoolWithoutUs = function(servers, req, extraPoolOptions) {

		 return re.GetPoolWithoutUs(re.GetRelayerPool(servers, req, [app.config.get("server_name")], extraPoolOptions))
	}




    re.GetTokenlessRelayerPool = function(servers, req, skipList, poolOptions) {

         if(!skipList) skipList = []

         var otherServers = []
         var skipped = []
         servers.forEach(server=>{

            if(skipList.indexOf(server.s_name) > -1) {
                skipped.push(server)
                return
            }

            var host = getServerFullHostname(server)

            otherServers.push({
                host: host,
                scheme: "https",
                auth_token: '-', // this is a special mode MonsterApiRelay will send the stuff
                extraHeaders: {
                    'mc-relayed': 1
                }
            })

         })


         var mc = getMapiPoolFor(otherServers, poolOptions)
         mc.length = otherServers.length
         mc.skipped = skipped
         return mc
    }
    re.GetTokenlessRelayerPoolWithoutUs = function(servers, req, extraPoolOptions) {

         return re.GetPoolWithoutUs(re.GetTokenlessRelayerPool(servers, req, [app.config.get("server_name")], extraPoolOptions));
    }


    re.GetPoolWithoutUs = function(mcPool) {
         mcPool.doCallNext = (mcPool.skipped.length > 0) // we can do it if the current server was among servers
         return mcPool
    }

	return re


}
