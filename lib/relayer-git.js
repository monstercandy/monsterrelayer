module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const webhosting_path_regexp = /^\/(\d+)\//
  
  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is are purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersGit", '/git')


  router.post(/^\/triggers\//, function(req,res,next){
     req.granted = true
     next()
  })

  router.use(function(req,res,next){
     if(req.granted) return next()

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_GIT")) req.granted = true

     if(req.granted) return next()

     // any pathes under /:webhosting
     var m = webhosting_path_regexp.exec(req.path)
     if(m) {
        req.params.webhosting = m[1]
        return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
     }

     next()
  })


  router.use(function(req,res,next){
  	 if(!req.granted)
  	 	 return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersGit", "/git"+req.path, req, next)
  })



  return router


}
