/*

this is just about binding the ADMIN_SMS subrole to the sms route.

*/

module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()

  function authorizeLocalWebstores(server_name_field_name, paramsMode){
     return function(req,res,next){
      // this info has been validated through MonsterExpress

      if(!req.TokenUser.session.IsGranted("ACCOUNT_LOCAL_WEBSTORES")) {
          throw new MError("PERMISSION_DENIED");
      }

      var originating_server = getOriginatingServer(req);
      var specifiedServer = paramsMode ? req.params[server_name_field_name] : req.body.json[server_name_field_name];
      if(specifiedServer != originating_server)
        throw new MError("INVALID_SERVER_NAME");

      return app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next);
    }
  }

  // the typical route to this method looks like this:
  // main accountapi server: PUT /s/targetserver/wizard/webhosting
  // target server: PUT /s/targetserver/wizard/webhosting
  // target server: PUT /s/targetserver/su/webhosting
  // target server: PUT /s/targetserver/su/accountapi/account/1/webhostings
  // main accountapi server: PUT /accountapi/account/1/webhostings
  router.put("/account/:user_id/webhostings", authorizeLocalWebstores("w_server_name"));
  router.delete("/account/:user_id/webhosting/:w_server_name/:webhosting_id", authorizeLocalWebstores("w_server_name", true));

  // this one is about querying domains attached to a webstore and removing them
  Array("post", "delete").forEach(cat=>{
     router[cat]("/webhostingdomains/by-wh", authorizeLocalWebstores("wd_server"));
  })

  router.post("/account/:user_id/sendmail/:email_category?", function(req,res,next){
      // this info has been validated through MonsterExpress

      if(!req.TokenUser.session.IsGranted("SEND_EMAIL_TO_USER")) {
          throw new MError("PERMISSION_DENIED");
      }

      return app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next);
  })

  /*
  // these ones are probably not needed (since the /account endpoint is redirected to the accountapi servers directly)
  router.get("/account/:user_id/domains", relayIf("INFO_ACCOUNT"));
  router.get("/account/:user_id/tels", relayIf("INFO_ACCOUNT"));
  router.get("/account/:user_id/emails", relayIf("INFO_ACCOUNT"));
  router.get("/account/:user_id/credentials", relayIf("INFO_ACCOUNT"));

  router.get("/email/categories", noRole());
  router.get("/tel/country_codes", noRole());
  */

  router.put("/escalations", relayIf("ESCALATE_EXCEPTION", false, function(req){
     app.assignOriginatingServer(req);
  }));

  router.put("/sms", relayIf("SEND_SMS", true));
  router.route("/sms",relayIfAdminSms());
  router.route("/config/sms",relayIfAdminSms());


  router.post("/authentication/credential", relayIf("INFO_ACCOUNT"));
  router.search("/webhostingtemplates", relayIf("INFO_WEBHOSTING_TEMPLATE"));

  router.put("/loginlog/events", function(req,res,next){
      // this info has been validated through MonsterExpress

      if(!req.TokenUser.session.IsGranted("LOGINLOG_EVENT_REPORT")) {
          throw new MError("PERMISSION_DENIED");
      }

      if(req.body.json.server) {
        var originating_server = getOriginatingServer(req);
        if(req.body.json.server != originating_server)
          throw new MError("INVALID_SERVER_NAME");        
      } else {
        app.assignOriginatingServer(req);
      }

      return app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next)
  });

  function getOriginatingServer(req){
      var originating_server = req.TokenUser.session.s_uuid.split(" ")[0];    
      return originating_server;
  }

  // --------------------------------------------- monsterinfo accountapi requests begin
  const RelayerInfoAccountLib = require("relayer-info-account.js");

  router.get("/account/:account_id", accountInfoRole(function(req){
    RelayerInfoAccountLib.filterNameUserInfoResponse(req.result);
    RelayerInfoAccountLib.filterNameUserInfoResponse(req.result);
  }));

  router.post("/account/:account_id", accountInfoRole(function(req){
    RelayerInfoAccountLib.filterMostUserInfoResponse(req.result);
  }));

  router.post("/account/:account_id/emails/:category", emailInfoRole());
  router.get ("/account/:account_id/emails/:category", emailInfoRole());

  router.post("/token", sessionInfoRole());

  router.get("/webhostingtemplate/:template", webhostingInfoTemplateRole());
  router.get("/webhostingtemplates", webhostingInfoTemplateRole());

  router.get("/servers", serversInfoRole());

  router.get("/config/webhostingtemplates/dynamiclanguages", noRole());

  // --------------------------------------------- monsterinfo accountapi requests end

  return router;

  function accountInfoRole(afterFilterCallback) {
     var relayIfLogic = relayIf("INFO_ACCOUNT");
     return function(req,res,next){
        return relayIfLogic(req,res, function(err){
           if(err) return next(err);
           afterFilterCallback(req);
           next();
        })
     }
  }


  function noRole(){
     return relayIf();
  }

  function emailInfoRole(){
     return relayIf("INFO_EMAIL");
  }

  function sessionInfoRole(){
     return relayIf("INFO_SESSION");
  }

  function webhostingInfoTemplateRole(){
    return relayIf("INFO_WEBHOSTING_TEMPLATE");
  }

  function serversInfoRole(){
    return relayIf("INFO_SERVERS");
  }

  function relayIfAdminSms(){
     return relayIf("ACCOUNT_SMS");
  }

  function relayIf(role, nextIfNoRole, callback) {
     return function(req,res,next){
         if((role)&&(!req.TokenUser.session.IsAnyGranted(["SUPERUSER", role]))) {
            if(nextIfNoRole) return next();
            return next(new MError("PERMISSION_DENIED"))
         }
         if(callback)
           callback(req);
         return app.Relay("ServersAccountapi", "/accountapi"+req.path, req, next)
     }
  }
}
