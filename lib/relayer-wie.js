
module.exports = function(app) {


      const subsystems = {

          // note: webhosting/docker shall be the first, they must be restored at first (other subsystems rely on them)
          "webhosting": "ServersWebhosting",
          "docker": "ServersDocker",

          "cert": "ServersCert",
          "dbms": "ServersDbms",
          "docrootapi": "ServersDocrootapi",
          "email": "ServersEmail",
          "fileman": "ServersFileman",
          "ftp": "ServersFtp",
          "git": "ServersGit",
      };
      const subsystemNames = Object.keys(subsystems);

      const writeFileOptions = {mode: 0o600};


      const vali = require("MonsterValidators").ValidateJs()
      const MError = require('MonsterError')

      const path = require('path');
      const dotq = require('MonsterDotq');
      const fs = dotq.fs();

      var router = app.ExpressPromiseRouter()

      router.use(function(req,res,next){
          if(!req.TokenUser.session.IsAnyGranted(["SUPERUSER", "ADMIN_WIE"])) return next(new MError("PERMISSION_DENIED"))

          return next();
      });


      router.get("/config/subsystems", function(req,res,next){
          return req.sendResponse(subsystemNames);
      })

      router.get("/webstores", function(req,res,next){
          return req.sendPromResultAsIs(readAllWieWebhostingIds());
      })

      router.get("/fetch", function(req,res,next){
          return req.sendPromResultAsIs(fetchAll());
      })

      router.get("/fetch/:webhosting_id", validateWebhosting, function(req,res,next){
          return req.sendPromResultAsIs(
            fetchWhJson(req.params.webhosting_id)
             .catch(ex=>{
                if(ex.code != "ENOENT") throw ex;

                return doBackup(req.params.webhosting_id, req)
                  .then(()=>{
                     return fetchWhJson(req.params.webhosting_id);
                  });
             })
          );
      })

      router.post("/feed", function(req,res,next){
         return req.sendOk(doFeed(null, req));
      })

      router.post("/feed/:webhosting_id", function(req,res,next){ // this one does not need validateWebhosting, because that parameter will be checked inside doFeed
         return req.sendOk(doFeed(req.params.webhosting_id, req));
      })

      router.post("/backup", function(req,res,next){
         return req.sendOk(doBackup(null, req));
      })

      router.post("/backup/:webhosting_id", validateWebhosting, function(req,res,next){
         return req.sendOk(doBackup(req.params.webhosting_id, req));
      })

      router.post("/restore", function(req,res,next){
         return req.sendOk(doRestore(null, req));
      })

      router.post("/restore/:webhosting_id", validateWebhosting, function(req,res,next){
         return req.sendOk(doRestore(req.params.webhosting_id, req));
      })


      install_cron();

      return router;


      function install_cron(){
        if(app.cron_wie) return;
        app.cron_wie = true;

        var csp = app.config.get("cron_wie_backup")
        if(!csp) return

        const cron = require('Croner');

        cron.schedule(csp, function(){
           console.log("wie backup everything");
           return doBackup()
            .catch(ex=>{
                console.error("Unable to create the scheduled backups", ex)
            })

        });
      }

      function doFeed(webhosting_id, req) {
        return Promise.resolve()
          .then(()=>{
             if(!webhosting_id) return removeExistingWieFiles();
          })
          .then(()=>{
             var payload = req.body.json;
             if(webhosting_id) {
               var new_payload = {};
               new_payload[webhosting_id] = payload;
               payload = new_payload;
             }

             return dotq.linearMap({array: Object.keys(payload), action: function(wh_id){
                return validateWebhostingInt(wh_id)
                 .then(()=>{
                    var content = toJsonStr(payload[wh_id]);
                    var wie = getWieFilePathForWebhosting(wh_id);
                    return fs.writeFileAsync(wie, content, writeFileOptions);
                 });
             }})
          })

      }


      function doBackup(webhosting_id, req) {
         if(!webhosting_id) webhosting_id = "";

         var aggregated = {};
         return dotq.linearMap({array: subsystemNames, action: subsystem=>{


            console.log(subsystem, ": generating new backup");
            var mapi = app.getMapiPoolAndSetupRequest(subsystems[subsystem], req, null, {dontCache: true})
            return mapi.getAsync("/"+subsystem+"/wie/"+webhosting_id)
              .catch(ex=>{
                 if((ex.message)&&(ex.message.match(/^SERVICE_NOT_CONFIGURED/)))
                    return;
                 throw ex;
              })
              .then(cre=>{
                 console.log(subsystem, ": backup generated");
                 if(!cre) return;

                 var c = cre.result;
                 if(!webhosting_id) {
                     Object.keys(c).forEach(wh_id=>{
                         aggregate(wh_id, c[wh_id]);
                     })

                 }
                 else
                    aggregate(webhosting_id, c);
              })

            function aggregate(wh_id, c) {
               if(!aggregated[wh_id])
                  aggregated[wh_id] = {};
               aggregated[wh_id][subsystem] = c;
            }

         }})
         .then(()=>{

            return dotq.linearMap({array: Object.keys(aggregated), action: wh_id=>{
                return fetchWhString(wh_id).catch(ex=>{})
                 .then(stored=>{
                     var actual = toJsonStr(aggregated[wh_id]);
                     if(stored == actual) return;

                     // else we need to store it
                     var wie = getWieFilePathForWebhosting(wh_id);
                     return fs.writeFileAsync(wie, actual, writeFileOptions);
                 })
            }})
         })

      }
       function toJsonStr(a){
          return JSON.stringify(a, null, 4);
       }

      function validateWebhostingInt(wh_id){
         return vali.async({wh_id: wh_id}, {wh_id: {presence: true, isInteger:true}})
      }

      function validateWebhosting(req,res,next){
         return validateWebhostingInt(req.params.webhosting_id)
           .then(()=>{
              next();
           })
      }

      function fetchWhString(wh_id){
          var wie = getWieFilePathForWebhosting(wh_id);
          return fs.readFileAsync(wie, "utf-8");
      }
      function fetchWhJson(wh_id){
         return fetchWhString(wh_id)
           .then(c=>{
              return JSON.parse(c);
           })
      }
      function getWieFilePathForWebhosting(wh_id) {
          var wie = app.getWieDirectory();
          return path.join(wie, wh_id+".json");
      }

      function readAllWieWebhostingIds(){
         var re = [];
         var wie = app.getWieDirectory();
         return fs.readdirAsync(wie)
           .then(files =>{
              var r = new RegExp('^([0-9]+)\.json$');
              files.forEach(file=>{
                  var m = r.exec(file);
                  if(!m) return;

                  var wh_id = m[1];
                  re.push(wh_id);
              })
              return re;
           })

      }

      function removeExistingWieFiles(){
         return readAllWieWebhostingIds()
           .then(wh_ids =>{
              return dotq.linearMap({array: wh_ids, action: wh_id=>{
                  var wie = getWieFilePathForWebhosting(wh_id);
                  return fs.unlinkAsync(wie);
              }})
           })

      }

      function fetchAll(){
         var re = {};
         return readAllWieWebhostingIds()
           .then(wh_ids =>{
              return dotq.linearMap({array: wh_ids, action: wh_id=>{

                  return fetchWhJson(wh_id)
                    .then(d=>{
                       re[wh_id] = d;
                    })
              }})
           })
           .then(()=>{
              return re;
           })
      }


      function doRestore(webhosting_id, req) {
         if(!webhosting_id) webhosting_id = "";

         var d;
         return Promise.resolve()
           .then(()=>{
              if(req)
                return vali.async(req.body.json, {subsystems: {inclusionCombo: subsystemNames}})
              return {};
           })
           .then((ad)=>{
              d = ad;
              if(!d.subsystems)
                 d.subsystems = subsystemNames;

              if(!webhosting_id)
                 return fetchAll();

              return fetchWhJson(webhosting_id);
           })
           .then(fullPayload=>{

             return dotq.linearMap({array: d.subsystems, action: subsystem=>{

                var mapi = app.getMapiPoolAndSetupRequest(subsystems[subsystem], req)

                var apayload;
                if(webhosting_id) {
                    apayload = fullPayload[subsystem];
                    if(!apayload) {
                       console.log("no backup available for subsystem", subsystem, "skipping")
                       return;
                    }
                } else {
                   apayload = {};
                   Object.keys(fullPayload).forEach(wh_id=>{
                      if(!fullPayload[wh_id][subsystem]) return;

                      apayload[wh_id] = fullPayload[wh_id][subsystem];
                   })
                }

                // console.log("doing restore with stuff", apayload)
                return mapi.postAsync("/"+subsystem+"/wie/"+webhosting_id, apayload)

             }})


         })

      }

}
