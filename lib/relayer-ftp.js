module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const webhosting_path_regexp = /^\/(\d+)\//

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})


  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_FTP")) req.granted = true

     next()

  })

  router.route("/sites/:webhosting/admin-login")
    .post(relayIfGranted)
  router.route("/sites/:webhosting/acls")
    .post(relayIfGranted)
    .put(relayIfGranted)
    .delete(relayIfGranted)

  router.route("/sites/tallies")
    .post(function(req,res,next){
        if(req.TokenUser.session.IsGranted("ADMIN_TALLY")) req.granted = true

        next()
    })

  router.route("/sites/admin-ips")
    .post(function(req,res,next){
        if(req.TokenUser.session.IsGranted("FEED_LOGIN_IP")) req.granted = true

        next()
    })

  router.route("/sites/:webhosting")
    .get(relayIfGranted)

  Array("status", "activate", "inactivate").forEach(c=>{
     router.post("/accounts/:webhosting/:username/totp/"+c, relayIfGranted)
  })
  router.post("/accounts/:webhosting/:username/ssh-authz", relayIfGranted)
  router.get ("/accounts/:webhosting/:username/ssh-authz", relayIfGranted)

  router.route("/accounts/:webhosting/:username")
    .get(relayIfGranted)
    .delete(relayIfGranted)

  router.route("/accounts/:webhosting")
    .put(relayIfGranted)
    .get(relayIfGranted)
    .search(relayIfGranted)
    .delete(relayIfGranted)

  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersFtp", "/ftp"+req.path, req, next)
  })

  function relayIfGranted(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
  }



  return router


}
