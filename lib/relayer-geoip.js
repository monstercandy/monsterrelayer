module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})


  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("INFO_GEOIP")) req.granted = true

     if(!req.granted) throw new MError("PERMISSION_DENIED");

     return next();
  })

  /*
  we could add some logic here to throttle
  */

  router.route("/lookup")
    .post(terminate);

  function terminate(req,res,next){
     return app.Relay("ServersMailer", "/geoip"+req.path, req, next);
  }

  return router

}
