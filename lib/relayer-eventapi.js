module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.put("/events", function(req,res,next){
      if(req.TokenUser.session.IsAnyGranted(["EMIT_LOG"])) req.granted = true
      return next();

  })

  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersEvent", "/eventapi"+req.path, req, next)
  })

  return router

}
