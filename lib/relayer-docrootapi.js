/*
POST:
        "/s/s1/su/docrootapi/apache/rehash": {"code":0, "stdout":"response from apache webserver", "stderr":"some warning"},
        "/s/s1/su/docrootapi/nginx/rehash": {"code":0, "stdout":"response from nginx webserver", "stderr":"some warning"},

        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/suspend": "ok, suspended",
        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/primary": "ok, set as primary",


GET:
                "/s/s1/docrootapi/docroots/12345/femforgacs.hu": {"entries":[{"host":"www.femforgacs.hu", "docroot": "/femforgacs.hu/pages"},{"host":"femforgacs.hu", "docroot": "/femforgacs.hu/pages"}]},
                "/s/s1/docrootapi/redirects/femforgacs.hu/": [{"host":"www.femforgacs.hu", "redirect": "http://index.hu"},{"host":"femforgacs.hu", "redirect": "http://origo.hu"}],

                "/s/s1/docrootapi/auths/12345/": [{"username":"user1"},{"username":"user2"}],

                "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": ["/dir1/", "/dir2/"],

                "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/raw": "some raw docroot config\ndata\nreally",
                "/s/s1/su/docrootapi/apache/12345/femforgacs.hu/raw": "some raw apache config\ndata\nreally",
                "/s/s1/su/docrootapi/nginx/12345/femforgacs.hu/raw": "some raw nginx config\ndata\nreally",



PUT:
              "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/": "ok",
              "/s/s1/docrootapi/redirects/femforgacs.hu/": "ok",
              "/s/s1/docrootapi/auths/12345/": "ok",

              "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": "ok",

DELETE:

    "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/www.femforgacs.hu":"ok",
    "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/femforgacs.hu":"ok",
    "/s/s1/docrootapi/redirects/femforgacs.hu/www.femforgacs.hu":"ok",
    "/s/s1/docrootapi/redirects/femforgacs.hu/femforgacs.hu":"ok",
    "/s/s1/docrootapi/auths/12345/user1": "ok",
    "/s/s1/docrootapi/auths/12345/user2": "ok",

    "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": "ok",

SEARCH:
    "/s/s1/su/docrootapi/docroots": [{"wh":"12345","domain":"femforgacs.hu"},{"wh":"12345","domain":"example.hu"}],

*/

module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")(app)

  var vali = require("MonsterValidators").ValidateJs()

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  // this is purposefully on the top
  app.SetupRouterForPipeTasks(router, "ServersDocrootapi", '/docrootapi')

  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_DOCROOTAPI")) req.granted = true

     next()
  })

  router.post("/awstats/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.delete("/awstats/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.post("/awstats/:webhosting/:domain/awstats_language", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))


  router.delete("/certificates/:cert_id",
    GrantHelper.relayIfCertAttach,
    function relayCertificateRequestIfBelongsToTheUser(req,res,next){

           // this means it has CERT_ATTACH, so we can relay
           if(req.granted) return next()

           return vali.async({certificate_id:req.params.cert_id}, {certificate_id:{isString:{strictName:true, lazy:true}}})
             .then(d=>{
                 var mapi = app.getMapiPoolAndSetupRequest("ServersCert", req);
                 return mapi.getAsync("/cert/certificates/by-user/"+req.TokenUser.account.u_id+"/item/"+d.certificate_id)
             })
             .then(cert=>{
                // it matches the account. (will throw some not found exception if it doesnt, which would be relayed back the user)
                req.granted = true
                return next()
             })

    }

  )


  Array(
    "httpsonly", "hstsheaders", "anticlickjackingheaders", "skipnginxstaticfiles", "nginxgzip", "forceredirect", "djangostatic",
  ).forEach(c=>{
     router.post("/docroots/:webhosting/:domain/"+c, GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant));
  })

  router.post("/docroots/:webhosting/:domain/certificate",
      // if the webhosting and the domain belong to the user...
      GrantHelper.relayIfCertAttach,
      GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant),
      function(req,res,next){
         if(!req.granted) return next()

         // if this request is coming from a trusted party, we dont need to verify if the certificate exists
         if(req.TokenUser.session.IsGranted("CERT_ATTACH")) return next()

         return vali.async(req.body.json, {certificate_id:{isString:{strictName:true, lazy:true}}})
           .then(d=>{
               var mapi = app.getMapiPoolAndSetupRequest("ServersCert", req);
               return mapi.getAsync("/cert/certificates/by-user/"+req.TokenUser.account.u_id+"/item/"+d.certificate_id)
           })
           .then(cert=>{
              // it matches the account. (will throw some not found exception if it doesnt, which would be relayed back the user)
              return next()
           })

      }
  )




  router.get("/protected-dirs/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.get("/docroots/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.get("/redirects/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.get("/frames/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.get("/auths/:webhosting", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
 
  router.post("/vts/:webhosting/search", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  router.post("/vts/:webhosting/search2", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))


     router.post("/docroots/webapps/:webhosting/upgrade", grantIfHasDocrootEntries);
     router.post("/docroots/webapps/upgrade", grantIfHasDocrootEntries);
     router.post("/docroots/webapps/:webhosting/detach", grantIfHasDocrootEntries);
     router.post("/docroots/webapps/detach", grantIfHasDocrootEntries);

     router.post("/docroots/webapps/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant), grantIfHasDocrootEntries, postDocrootWebappCommonLogic);
     router.post("/docroots/webapps/:webhosting", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant), grantIfHasDocrootEntries, postDocrootWebappCommonLogic);

   function postDocrootWebappCommonLogic(req,res,next){
       if(!req.granted) return next();
       if(!req.granted_docroot_entries) {
          if((!req.body.json.type)||(!req.body.json.type.match(/^(default|none|host-legacy)$/))) {
             throw new MError("PERMISSION_DENIED");              
          }
       }

       return next();
   }

  router.get("/docroots/webapps/:webhosting", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant), grantIfHasDocrootEntries, function(req,res,next){
     if(!req.granted) return next();

     return app.Relay("ServersDocrootapi", "/docrootapi"+req.path, req, function(err){
        if(err) return next(err);
        // {"vhosts":{"hacktest.test2.monstermedia.hu":{"type":"default"}},"default":{"type":"host-legacy"}}}
        var res = req.result;
        if(!req.granted_docroot_entries) {
          Array("unix_socket_path").forEach(q=>{
              delete res.default[q];
              Object.keys(res.vhosts).forEach(vhost=>{
                 delete res.vhosts[vhost].unix_socket_path;
              });
          });

        }
        
        return req.sendResponse(res);
     })
  })

  router.get("/docroots/:webhosting/", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))


  router.get("/docroots/hostentries/:webhosting/", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant), grantIfHasDocrootEntries)
  Array("hostmap","distinct","certificates","webapps").forEach(cat=>{
     router.get("/docroots/"+cat+"/:webhosting/", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  })


  router.put("/docroots/:webhosting/:domain/entries", function(req,res,next){
       delete req.body.json.force

       // note: this is needed as a new domain might have been just added
       return req.TokenUser.refetchToken().then(next);
    },
    GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant),
    grantIfHasDocrootEntries)
  router.put("/protected-dirs/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.put("/redirects/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.put("/frames/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.put("/auths/:webhosting", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))


  router.delete("/docroots/:webhosting/:domain/entries/:entry", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.delete("/redirects/:domain/:entry", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.delete("/frames/:domain/:entry", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.delete("/auths/:webhosting/:username", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  router.delete("/protected-dirs/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))

  router.delete("/docroots/:webhosting/:domain", GrantHelper.grantChecker(GrantHelper.perDomainWebhostingGrant))
  router.delete("/redirects/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))
  router.delete("/frames/:domain", GrantHelper.grantChecker(GrantHelper.perDomainGrant))


  router.use(function(req,res,next){
  	 if(!req.granted)
  	 	return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersDocrootapi", "/docrootapi"+req.path, req, next)
  })



  return router


  function grantIfHasDocrootEntries(req,res,next){
     if((!req.granted)&&(req.TokenUser.session.IsGranted("DOCROOT_ENTRIES"))) {
        req.granted = true;
        req.granted_docroot_entries = true;
     }
     return next();
   }


}
