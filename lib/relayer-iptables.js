module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})
  var vali = require("MonsterValidators").ValidateJs()

// this is purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersIptables", '/iptables')

  router.put("/bans/:category", function(req,res,next){
     if(req.TokenUser.session.IsAnyGranted(["BAN_IP","ADMIN_IPTABLES","SUPERUSER"])) req.granted = true
     return next()
  })

  router.post("/hostssh", function(req,res,next){
     if(req.TokenUser.session.IsAnyGranted(["FEED_HOSTSSH_IP","ADMIN_IPTABLES","SUPERUSER"])) req.granted = true
     return next()
  })

  router.put("/whextra/:webhosting/firewallrules", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant), function(req,res,next){
     if(!req.granted) return next()

      // this constraint here rejects ips from private ranges, zero ip and other malicious ones
     return vali.async(req.body.json, {"fw_dst_ip": {presence: true, isValidIPv4: {canBeRange: true,typicalRanges:true}}})
       .then(()=>{
          next()
       })
  })

  Array("get", "delete" ).forEach(method=>{
    router[method]("/whextra/:webhosting/firewallrules", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  })
  router.post("/whextra/:webhosting/firewallrules/logging", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  router.post("/whextra/:webhosting/firewallrules/status", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))
  router.delete("/whextra/:webhosting/firewallrules/rule", GrantHelper.grantChecker(GrantHelper.perWebhostingGrant))

  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersIptables", "/iptables"+req.path, req, next)
  })

  return router

}
