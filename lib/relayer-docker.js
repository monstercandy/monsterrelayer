module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const webhosting_path_regexp = /^\/(\d+)\//
  
  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is are purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersDocker", '/docker', [/^\/shellinabox\//])


  function relayIfGrantedWebhosting(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
  }

  router.use("/webhosting/:webhosting/",  relayIfGrantedWebhosting, function(req,res,next){
     if(!req.granted)
       return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersDocker", "/docker/webhosting/"+req.params.webhosting+req.path, req, next)
  })


  return router

}
