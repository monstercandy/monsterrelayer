
module.exports = function(app) {

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const MError = require('MonsterError')
  const dotq = require("MonsterDotq");

  var router = app.ExpressPromiseRouter()

  var vali = require("MonsterValidators").ValidateJs()




  router.get("/services/webhostings/details",function(req,res,next){
      /* We will return something like this:
        [
                    {w_server_name: "s1", w_webhosting_id: 12345, wh: {wh_name: "foobar"}},
                    ...
        ]
      */
      var d = getWebhostingArrayCopy(req);
      var re = [];
      var relayerWithTokenFromOriginalRequest = app.getMapiRelayer(req);
      return dotq.linearMap({array:d, catch:true, action: function(row){
          return relayerWithTokenFromOriginalRequest.getAsync("/s/"+row.w_server_name+"/webhosting/"+row.w_webhosting_id)
            .then(d=>{
               row.wh = d.result;
               re.push(row);
            })
      }})
      .then(()=>{
         return req.sendResponse(re);
      })

  });

  router.get("/services/webhostings",function(req,res,next){
      // We will return something like this:
      // [{"w_server_name":"s1","w_webhosting_id":16387},...]

      return req.sendResponse(req.TokenUser.webhostings);
  });

  return router


  function getWebhostingArrayCopy(req){
     var d = simpleCloneObject(req.TokenUser.webhostings);
     return d;
  }

}
