module.exports = function(app) {

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){
     return app.Relay("ServersEmail", "/email/pub"+req.path, req, function(err){
         if(err) return next(err)
         req.sendResponse(req.result)
     })
  })

  return router

}
