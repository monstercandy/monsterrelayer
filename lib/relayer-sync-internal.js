
// note: this is the deprecated approach when there are multiple instances of the underlying services configured,
// for example two internal tapi instances without a relayer on the second host

module.exports = function(app) {


  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()

  const timeoutOptions = {config:{timeout: app.config.get("sync_timeout")}}
  Array(
     {"name": "tapi", "grants": ["ADMIN_TAPI"], "serversRouter": "ServersTapi"},
     {"name": "accountapi", "grants": ["ADMIN_ACCOUNTAPI"], "serversRouter": "ServersAccountapi"}
  ).forEach(conf=>{

      router.post("/"+conf.name, function(req,res,next){

         if(!req.TokenUser.session.IsAnyGranted(allPerms(conf.grants))) return next(new MError("PERMISSION_DENIED"))
         
         var rawServers = app[conf.serversRouter].GetRawServers()
         if(rawServers.length < 2) return next(new MError("NOT_ENOUGH_SERVERS_CONFIGURED"))

         var dst = rawServers.slice()
         var src = dst.splice(0,1)
         var srcRouter = {
            GetRawServers: function() {return src}
         }
         return app.Mapi.getMapiPool(srcRouter).getAsync("/"+conf.name+"/export/tgz", timeoutOptions)
           .then(getRes=>{
                var dstRouter = {GetRawServers: function(){return dst}}

                return app.Mapi.getMapiPool(dstRouter).postAsync("/"+conf.name+"/import/tgz", getRes.result, timeoutOptions)
            })
           .then(postRes=>{
              req.result = postRes.result
              next()
           })

      })

  })



  return router

  function allPerms(additionalPermissions) {
     return ["SUPERUSER"].concat(additionalPermissions||[])
  }

}
