
module.exports = function(app, mclientPoolCache) {

  
  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){
      if(!req.TokenUser.session.IsGranted("FLUSH_CACHE")) return next(new MError("PERMISSION_DENIED"))

      next()
  })

  router.post("/token", function(req,res,next){

      app.DeleteTokenFromCache(req)
      req.sendOk()
  })

  router.post("/mapi/:servername", function(req,res,next){

      if((!app[req.params.servername]) || (!app[req.params.servername].GetRawServers))
        throw new MError("SERVER_MIDDLEWARE_NOT_FOUND")

      var servers = app[req.params.servername].GetRawServers()
      servers.forEach(server=>{
          delete server.mapiClient
      })

      req.sendOk()
      
  })



  return router


}
