module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()

  const MError = require('MonsterError')

  const webhosting_path_regexp = /^\/(\d+)\//

  var vali = require("MonsterValidators").ValidateJs()

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersFileman", '/fileman')

// it seems we relay everything here, since there is nothing to be protected inside

  router.use(function(req,res,next){

     if(req.TokenUser.session.IsSuperUser()) req.granted = true
     else if(req.TokenUser.session.IsGranted("ADMIN_FILEMAN")) req.granted = true
     else if(req.TokenUser.session.IsGranted("FILEMAN_UPLOAD_DBBACKUP")) {
       if(req.path.match(/\/upload$/))
          v = {dst_full_path: {presence:true, isPath: {noPathCharacterRestrictions: true}, format: {pattern: /^\/dbbackup\/.+/}}}
       else
       if(req.path.match(/\/find$/))
          v = {path: {presence:true, format: {pattern: /^\/dbbackup\/$/}}}
       else
         throw new MError("DBBACKUP_INVALID_OPERATION")

       return vali.async(req.body.json, v)
         .then(d=>{
            req.granted = true
            return next()
         })
     }
     else if(req.TokenUser.session.IsGranted("FILEMAN_MAILDIRMAKE")) {

       if(!req.path.match(/\/(remove|maildirmake)$/))
         throw new MError("MAILDIRMAKE_INVALID_OPERATION")

       return vali.async(req.body.json, {path_category:{presence:true, inclusion:["mail"]}})
         .then(d=>{
            req.granted = true
            return next()
         })

     }
     else {
         var m = webhosting_path_regexp.exec(req.path)
         if(m) {
            req.params.webhosting = m[1]
            return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
         }

     }

     next()
  })

  Array("untgz","untar").forEach(c=>{
     router.post("/:webhosting_id/"+c+"/transfer/", transferHandler(c));
  })

  // this is a special case, further validation is needed to see whether the url submitted belongs to the user indeed:
  router.put("/:webhosting_id/cron/", function(req,res,next){
      if(!req.granted) return

      req.body.json.user_id = req.TokenUser.account.u_id

      return vali.async(req.body.json, {
         docroot_domain:{presence: true, isHost: true},
         data:{presence:true,url:true},
      })
      .then(d=>{
         return app.getMapiRelayer(req).getAsync("/s/"+req.params.server+"/docrootapi/docroots/"+req.params.webhosting_id+"/"+d.docroot_domain.d_host_canonical_name)
      })
      .then(response=>{
         req.granted = false
         var urlToBeValidated = req.body.json.data
         response.result.entries.some(x=>{
            if(
               (urlToBeValidated.indexOf("http://"+x.host+"/") == 0)
               ||
               (urlToBeValidated.indexOf("https://"+x.host+"/") == 0)
              )
            {
              req.granted = true
              return true
            }
         })

         return next()
      })
  })

  router.use(function(req,res,next){

     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     return app.Relay("ServersFileman", "/fileman"+req.path, req, next)
  })




  return router


  function transferHandler(category){
    return function(req,res,next){
       if(!req.granted) return next();

       // this is a special case; we receive a fileman download id here
       // which needs to be retrieved and relayed to the fileman destination.
       // the destination is already validated at this point

       var d;
       var emitter;
       var uploadHandler;
       return vali.async(req.body.json, {
          source_server: {presence: true, isString: {strictName: true}},
          source_download_id: {presence: true, isString: {hex: true}},
          dst_dir_path: {presence:true, isPath: {noPathCharacterRestrictions: true}},
          path_category: {isString: true},
       })
       .then(ad=>{
          d = ad;
          emitter = app.commander.EventEmitter();
          return emitter.spawn({
              omitAggregatedOutput: true,
              removeImmediately: true,
          })

       })
       .then(eh=>{
            req.sendTask(Promise.resolve(eh));

            emitter.send_stdout_ln("Initiating upload...");

            var mapi = app.getMapiPoolAndSetupRequest("ServersFileman", req);
            return mapi.postAsync("/fileman/"+req.params.webhosting_id+"/"+category, {
              dst_dir_path: d.dst_dir_path, 
              filename: "transfer."+category,
              path_category: d.path_category,
            })
        })
        .then((re)=>{
            uploadHandler = re.result;

            // this must be routed through the relayer
            var downloadUrl = app.getLocalRelayerUrlPrefix() + "/s/"+d.source_server+"/fileman/tasks/"+ d.source_download_id;

            // and this goes to the local internal instance directly
            var uploadUrl = app.constructUrlFromServer(app.ServersFileman.FirstServerOrThrow())+"/fileman/tasks/"+uploadHandler.id+"/";

            setTimeout(function(){
                const request = require("RequestCommon");

                emitter.send_stdout_ln("Transferring files between storages...");

                var uploadInputUrl= uploadUrl+"input"

                console.error("Transferring between", downloadUrl, "and", uploadInputUrl);

                var source = request.get(downloadUrl);
                source.on("error", function(err){
                   console.error("Error while retrieving the data", err);
                   emitter.send_stdout_ln("Error while fetching the data");
                   emitter.close(1);
                })

                var destination = request.post(uploadInputUrl)
                destination.on("error", function(err){
                   console.error("Error while uploading the data", err);
                   emitter.send_stdout_ln("Error while uploading the data");
                   emitter.close(1);
                })
                source.pipe(destination);

            }, 1000);

            return emitter.relayUrl(uploadUrl+"output")
              .catch(ex=>{
                  console.error("error while relaying the transfer task", ex);
                  emitter.send_stdout_ln("Error while transferring the data");
                  emitter.close(1);
              })

        })
        .then(()=>{
            emitter.send_stdout_ln("Upload has finished successfully.");
            emitter.close();
        })

    }
  }

}
