module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})


  router.use(function(req,res,next){

     if(
         ((req.body.json.toAdmin)&&(!req.body.json.to)&&(!req.body.json.toAccount))
         ||
         (req.TokenUser.session.IsSuperUser())
         ||
         (req.TokenUser.session.IsGranted("SEND_EMAIL")) 
       ) 
     {
       req.granted = true
     }

     if(!req.granted) throw new MError("PERMISSION_DENIED");

     return next();
  })

  /*
  we could add some logic here to throttle
  */

  router.route("/sendmail")
    .post(terminate)

  function terminate(req,res,next){
     return app.Relay("ServersMailer", "/mailer"+req.path, req, next);
  }

  return router

}
