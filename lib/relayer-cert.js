module.exports = function(app) {

  const GrantHelper = require("grant-helper.js")()
  var vali = require("MonsterValidators").ValidateJs()


  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

// this is purposefully on the top, they dont need req.TokenUser to be present, since they are public
  app.SetupRouterForPipeTasks(router, "ServersCert", '/cert')


  router.use("/info", function(req,res,next){
      req.granted = true
      next()
  })

  router.get("/config/countries", function(req,res,next){
    req.granted = true
    next()
  })


  Array(
      "/certificates/by-webhosting/:webhosting/item/:cert_id/disable",
      "/certificates/by-webhosting/:webhosting/item/:cert_id/attach",
      "/certificates/by-webhosting/:webhosting/item/:cert_id/detach",
      "/certificates/by-webhosting/:webhosting/item/:cert_id/intermediates",
      "/certificates/by-webhosting/:webhosting/search"
    ).forEach(url=>{
        router.all(url, relayIfGrantedWebhosting)

    })

  router.put("/certificates/by-webhosting/:webhosting/", GrantHelper.relayIfCertAttach, relayIfGrantedWebhosting)
  router.get("/certificates/by-webhosting/:webhosting/", relayIfGrantedWebhosting)
  router.get("/certificates/by-webhosting/:webhosting/item/:cert_id/private-key", relayIfGrantedWebhosting)
  router.get("/certificates/by-webhosting/:webhosting/item/:cert_id", relayIfGrantedWebhosting)

  router.post("/certificates/by-webhosting/:webhosting/item/:cert_id/copy", relayIfGrantedWebhosting, copyLogic);


  Array(
      "/certificates/my/item/:cert_id/disable",
      "/certificates/my/item/:cert_id/attach",
      "/certificates/my/item/:cert_id/detach",
      "/certificates/my/item/:cert_id/intermediates",
      "/certificates/my/search"
    ).forEach(url=>{
       router.all(url,rewriteCertPathToUserAndRelay)

    })

  router.put("/certificates/my/", rewriteCertPathToUserAndRelay)
  router.get("/certificates/my/", rewriteCertPathToUserAndRelay)
  router.get("/certificates/my/item/:cert_id/private-key", rewriteCertPathToUserAndRelay)
  router.post("/certificates/my/item/:cert_id/copy", rewriteCertPathToUserAndRelay, copyLogic)
  router.post("/certificates/my/item/:cert_id/disable", rewriteCertPathToUserAndRelay)
  router.get("/certificates/my/item/:cert_id", rewriteCertPathToUserAndRelay)

  router.put("/contacts/my/", rewriteContactPathToUserAndRelay)
  router.get("/contacts/my/", rewriteContactPathToUserAndRelay)
  router.get("/contacts/my/item/:contact_id", rewriteContactPathToUserAndRelay)
  router.delete("/contacts/my/item/:contact_id", rewriteContactPathToUserAndRelay)


  router.get( "/purchases/my", rewritePurchasePathToUserAndRelay)

  router.get( "/purchases/my/item/:item", rewritePurchasePathToUserAndRelay)

  router.put( "/purchases/my/letsencrypt", function(req,res,next){
     return attachValidator(req, true).then(next)

  }, rewritePurchasePathToUserAndRelay)

  router.get( "/purchases/my/item/:item/activation", rewritePurchasePathToUserAndRelay)

  router.post( "/purchases/my/item/:item/reissue", rewritePurchasePathToUserAndRelay)
  router.post( "/purchases/my/item/:item/reissue/task", rewritePurchasePathToUserAndRelay)
  router.post( "/purchases/my/item/:item/revoke", rewritePurchasePathToUserAndRelay)
  router.post( "/purchases/my/item/:item/dispatch/:method", rewritePurchasePathToUserAndRelay)
  router.post( "/purchases/my/item/:item/dispatch", rewritePurchasePathToUserAndRelay)

  router.post( "/purchases/my/item/:item/activate/task", attachValidatorLogic, rewritePurchasePathToUserAndRelay);
  router.put ( "/purchases/my/item/:item/activations",   attachValidatorLogic, rewritePurchasePathToUserAndRelay);

  router.use(function(req,res,next){
     if(!req.granted)
        return next(new MError("PERMISSION_DENIED"))

     if(req.body.json)
        req.body.json.c_user_id = req.TokenUser.account.u_id

     return app.Relay("ServersCert", req.newPath || "/cert"+req.path, req, function(err){
         if(err) return next(err)

        if((req.result)&&(req.result.purchases)) {
           req.result.purchases.forEach(pc=>{
              delete pc.pc_reseller
              delete pc.pc_reseller_certificate_id
           })
        }
        return next()
     })
  })

  function relayIfGrantedWebhosting(req,res,next){
     if(req.granted) return next()
     return GrantHelper.grantChecker(GrantHelper.perWebhostingGrant)(req,res,next)
  }


  return router

  function attachValidatorLogic(req,res,next){

     return attachValidator(req, false).then(next)

   }

  function rewriteCertPathToUserAndRelay(req,res,next){
     var r = /^\/certificates\/my(.*)/
     var m = r.exec(req.path)
     if((!m)||(!m[1])) return next()

     req.granted = true
     req.newPath = "/cert/certificates/by-user/"+req.TokenUser.account.u_id+m[1]
     return next()
  }

  function rewriteContactPathToUserAndRelay(req,res,next){
     var r = /^\/contacts\/my(.*)/
     var m = r.exec(req.path)
     if((!m)||(!m[1])) return next()

     req.granted = true
     req.newPath = "/cert/contacts/by-user/"+req.TokenUser.account.u_id+m[1]
     return next()
  }

  function rewritePurchasePathToUserAndRelay(req,res,next){
     var r = /^\/purchases\/my(.*)/
     var m = r.exec(req.path)
     if(!m)return next()
     if(!m[1]) m[1] = ""

     req.granted = true
     req.newPath = "/cert/purchases/by-user/"+req.TokenUser.account.u_id+m[1]
     return next()
  }

  function copyLogic(req,res,next){
     if(!req.granted) return

     var newpath
     if(req.params.webhosting)
        newpath = "/cert/certificates/by-webhosting/"+req.params.webhosting+"/item/"+req.params.cert_id+"/full"
     else
        newpath = "/cert/certificates/by-user/"+req.TokenUser.account.u_id+"/item/"+req.params.cert_id+"/full"

     req.method = "GET"
     return app.Relay("ServersCert", newpath, req, function(err){
        if(err) return next(err)
        var cert = req.result
        delete req.result

        delete cert.c_id

        // this uglyness is needed for the upload
        Object.keys(cert.data).forEach(x=>{
           cert[x] = cert.data[x]
           delete cert.data[x]
        })

        return app.getMapiRelayer(req).putAsync("/s/"+req.body.json.server+"/cert/certificates/by-webhosting/"+req.body.json.webhosting_id, cert)
          .then(res=>{
              // terminating here, no more next()
              res.result = res.result
              req.sendResponse(res.result)
          })
     })

  }


  function attachValidator(req, enforce){
    var d
    return vali.async(req.body.json, {attach:{presence: enforce, isSimpleObject: true}})
      .then(ad=>{
         d = ad

         if(!d.attach) return Promise.resolve()

         var whateverAttach;

         return vali.async(d.attach, {
            server: {presence: true, isString: {strictName: true}, inclusion: [req.params.server]},
            webhosting_id: {presence: true, isInteger: {lazy:true}},
            docroot_domain: {presence: true, isHost: true}
         })
         .then(attach=>{

             whateverAttach = attach;

             if(!GrantHelper.doesWebhostingBelongToUser(req, whateverAttach.server, whateverAttach.webhosting_id))
               throw new MError("PERMISSION_DENIED")

             return app.getMapiRelayer(req).getAsync("/s/"+req.params.server+"/docrootapi/docroots/"+whateverAttach.webhosting_id+"/"+whateverAttach.docroot_domain.d_host_canonical_name)
         })
         .then(response=>{
            // it returns something like this: "/s/s1/docrootapi/docroots/12345/femforgacs.hu": {"entries":[{"host":"www.femforgacs.hu", "docroot": "/femforgacs.hu/pages"},{"host":"femforgacs.hu", "docroot": "/femforgacs.hu/pages"}]},

             req.body.json.attach = d.attach
             req.body.json.domains = []
             response.result.entries.forEach(e=>{
                req.body.json.domains.push(e.host)
             })

             return app.getMapiRelayer(req).getAsync("/s/"+req.params.server+"/docrootapi/redirects/"+whateverAttach.docroot_domain.d_host_canonical_name)
         })
         .then(response=>{
            // it returns something like this: "/s/s1/docrootapi/docroots/12345/femforgacs.hu": [{"host":"www.gitarkurzusok.hu","redirect":"https://gitarkurzusok.hu"},{"host":"gitarkurzusok.hu","redirect":"https://gitarkurzusok.hu"}]

             response.result.forEach(e=>{
                if(req.body.json.domains.indexOf(e.host) < 0) {
                   req.body.json.domains.push(e.host)  
                }
             })

             if(!req.body.json.domains.length)
                throw new MError("NO_HOSTS_CONFIGURED")

         })


      })

  }


}
