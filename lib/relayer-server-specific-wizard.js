/*
TODO:
change webhosting template from to (across servers)
remove webhosting template (if there are no more webhostings assigned to it)

create domain (including server and webhosting specification, dns zone creation, accountapi permission grant)
remove domain (including accountapi permission revoke)
*/

module.exports = function(app) {

  // tapi business logic towards tapi is enforced here
  // req.TokenUser

  const MError = require('MonsterError')

  var vali = require("MonsterValidators").ValidateJs()


  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){
     // this is a local stuff, relaying to this MonsterRelayer instance using the token from the original request
     req.relayerWithTokenFromOriginalRequest = app.getMapiRelayer(req)
     next()
  })

   router.get("/test",function(req,res,next){
        return req.relayerWithTokenFromOriginalRequest.postAsync("/s/mocha/test", {"hey":"world"})
          .then(response=>{
             // console.log("response received", response.result)
             req.sendResponse(response.result)
          })

     })


   function validateWebhostingsParamaters(req, extras){
       if(!extras) extras = {}
       extras.wh_id = { presence: true, isInteger: true }

       return vali.async(req.body.json, extras)
   }


   function changeWebhostingParametersThenRebuildDocroots(req, extras, asSuperuser, suspendEmailAsWell) {
         extras.wh_id = { presence: true, isInteger: true }

         var d
         var p = validateWebhostingsParamaters(req, extras)
              .then((aD)=>{
                  d = aD

                  // authorization relies in this call, where the permissions will be checked behind the token
                  return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+req.params.server+(asSuperuser?"/su":"")+"/webhosting/"+d.wh_id, d)

              })
              .then(()=>{
                  if(!suspendEmailAsWell) return;

                  var mapi = app.getMapiPoolAndSetupRequest("ServersEmail", req);
                  return mapi.postAsync("/email/webhostings/"+d.wh_id+"/", {do_suspended: !extras.wh_is_expired})
              })
              .then(()=>{
                  // NOTE: this call here is not bound to any permissions (using internal api), since the previous relayed call

                  var mapi = app.getMapiPoolAndSetupRequest("ServersDocrootapi", req)

                  return mapi.postAsync("/docrootapi/cleanup/webhosting/"+d.wh_id+"/rebuild", {})
                    .catch(ex=>{
                        if(ex.message == "DOCROOT_NOT_FOUND")
                           return;
                        throw ex
                    })
              })

         return req.sendOk(p)
   }


   router.route("/webhosting/php")
     .post(function(req,res,next){
         return changeWebhostingParametersThenRebuildDocroots(req, { wh_php_version:   { presence: true } } )
     })

   router.route("/webhosting/expired")
     .post(function(req,res,next){
         return changeWebhostingParametersThenRebuildDocroots(req, { wh_is_expired:   { presence: true, isBooleanLazy: true } }, true, true )
     })

   function installatronCommon(req, d){
        var webhosting
        var ftpuser;
        var vhosts = {}
        var databases = []

        // d.server is where the target webhosting site resides

        var target_hostname = app.ServersLib.getFullHostname(d.server)

        return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+d.server+"/webhosting/"+d.wh_id)
          .then(whResponse=>{
             webhosting = whResponse.result
             if(!webhosting.template.t_installatron_allowed)
                throw new MError("INSTALLATRON_NOT_ALLOWED")

             if(webhosting.wh_installatron_server) return;

             // no installatron server is assigned so far, lets find one (randomly?)
             return app.ServersLib.GetServers("installatron")
                .then(servers=>{
                   var server = app.ServersLib.GetServerRandomly(servers);

                   webhosting.wh_installatron_server = server.s_name;

                   var mapi = app.getMapiPoolAndSetupRequest("ServersWebhosting", req);

                   return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+d.server+"/webhosting/"+d.wh_id, 
                      {wh_installatron_server: webhosting.wh_installatron_server}
                   );

                })
                .then(()=>{
                   return Promise.resolve()
                })

          })
          .then(()=>{
             return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+d.server+"/ftp/accounts/"+d.wh_id)
          })
          .then((ftpResponse)=>{

             var ftpAccounts = ftpResponse.result;
             ftpAccounts.some(ftpAccount=>{
                if(ftpAccount.fa_subdir != "/") return;

                ftpuser = ftpAccount.fa_username;
                return true;
             })

             if(!ftpuser) throw new MError("NO_PROPER_FTP_ACCOUNT_CONFIGURED_FOR_INSTALLATRON");

             return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+d.server+"/docrootapi/docroots/hostmap/"+d.wh_id);

          })
          .then((docrootResponse)=>{
              var docroots = docrootResponse.result
              var added = 0
              Object.keys(docroots).forEach(hostname=>{
                  vhosts["http://"+hostname] = docroots[hostname]
                  added++
              })

              if(!added) throw new MError("NO_DOCROOTS_CONFIGURED_FOR_INSTALLATRON")

              return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+d.server+"/dbms/databases/"+d.wh_id)
          })
          .then((dbmsResponse)=>{
              var dbmsDatabases = dbmsResponse.result

              dbmsDatabases.forEach(dbmsDatabase=>{
                 databases.push({
                   type: dbmsDatabase.dm_dbms_type,
                   host: target_hostname,
                   name: dbmsDatabase.db_database_name,
                   user: dbmsDatabase.db_database_name,
                 })

              })

              if(databases.length <= 0) throw new MError("NO_DATABASES_CONFIGURED_FOR_INSTALLATRON")

          })
          .then(()=>{
             var re = {
                 // this one is actually a helper only
                installatron_server_name: webhosting.wh_installatron_server,

                webhosting_id: d.wh_id,
                server: d.server,
                ftp_username: ftpuser,
                
                label: webhosting.wh_name,
                vhosts:vhosts,
                databases: databases,
                language: req.TokenUser.account.u_language,
                email: req.TokenUser.account.u_primary_email,
             }

             console.log("installatron guixfer request consturcted", re);
             return re;
          })
   }

   // they are the kickoff calls, that are invoked on the same server where the webhosting package is
   router.route("/webhosting/installatron/query")
     .post(function(req,res,next){
        return validateInstallatronParameters(req)
          .then(d=>{
             d.server = req.params.server;
             return installatronCommon(req, d);
          })
          .then(allData=>{
             return req.sendResponse({installatron_server: allData.installatron_server_name})
          })
     })
   router.route("/webhosting/installatron/kickoff/raw")
    .post(function(req,res,next){
       var allData = req.body.json;

       var mapi = app.getMapiPoolAndSetupRequest("ServersInstallatron", req)
       return req.sendPromResultAsIs(
          mapi.postAsync("/installatron/webhosting/guixfer", allData)
          .then(response=>{
              return response.result;
          })
       )

    })
   router.route("/webhosting/installatron/kickoff")
    .post(function(req,res,next){

        return req.sendPromResultAsIs(
         validateInstallatronParameters(req)
          .then(d=>{
              return installatronCommon(req, d)
          })
          .then((allData)=>{

             return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+allData.installatron_server_name+"/wizard/webhosting/installatron/kickoff/raw", allData);
          })
          .then(response=>{
             return response.result;
          })

         )
     })

  function validateInstallatronParameters(req) {
     return validateWebhostingsParamaters(req, {server: {presence:true, isString: {strictName: true} } });
  }


   router.route("/webhosting")
     .put(userSpec, function(req,res,next){

        var whResponse
        var p = req.relayerWithTokenFromOriginalRequest.putAsync("/s/"+req.params.server+"/su/webhosting/", req.body.json)
          .then(awhResponse=>{
              whResponse = awhResponse.result

              // at this point the user has proved that he/she has permissions to create a webstore on this particular target server.

              // now we need to store this information in accountapi among the permissions.

              return new Promise(function(resolve,reject){
                  var customPayload = {"w_server_name": req.params.server, "w_webhosting_id": whResponse.wh_id};
                  req.noUriTransform = true;
                  return app.Relay("ServersAccountapi", "/accountapi/account/"+req.wh_user_id+"/webhostings", req, function(err){
                     delete req.noUriTransform;

                     if(err) return reject(err);
                     return resolve();
                  }, customPayload);
              })

          })
          .then(()=>{

              if(req.body.json.no_email_service_account) return;

              var this_server_name = app.config.get("server_name");
              var service_account_tld = app.config.get("email_service_account_tld");

              var domain_name = whResponse.wh_id+"."+this_server_name+service_account_tld;
              var email_full = app.config.get("email_service_account_username")+"@"+domain_name;

              var ea_id;
              var password;

              const Token = require("Token");
              return Token.GetTokenAsync()
                .then((aPassword)=>{
                    password = aPassword;

                    return req.relayerWithTokenFromOriginalRequest.putAsync(
                      "/s/"+req.params.server+"/su/email/domains/",
                      { 
                         account: req.TokenUser.account.u_id,
                         do_webhosting: whResponse.wh_id,
                         do_domain_name: domain_name
                      }
                    );
                  
                })
                .then(()=>{
                     var params = {
                         "account": req.TokenUser.account.u_id,
                         "ea_email": email_full,
                         "ea_password": password,
                         "ea_quota_bytes": 1, // 0 means unlimited, so we need to limit it this way (it is not supposed to receive any emails)
                        }

                     return req.relayerWithTokenFromOriginalRequest.putAsync("/s/"+req.params.server+"/su/email/domains/domain/"+domain_name+"/accounts", params)
                })
                .then((res)=>{
                   ea_id = res.result.ea_id;

                   var policy = whResponse.template.t_webapp_email_quota_policy;
                   if(!policy) return;

                   return req.relayerWithTokenFromOriginalRequest.putAsync(
                     "/s/"+req.params.server+"/su/email/cluebringer/policies/outbound/"+policy+"/members", 
                     {ea_email: email_full})
                     .catch(ex=>{
                        console.error("Unable to assign the required policy for email", ex)
                        if(!req.body.json.ignoreMailPolicyErrors) 
                           throw ex;
                     })

                   ;
                })
                .then(()=>{
                     const webapp = app.config.get("docker_storage_default_webapp");
                     if(!webapp) return;

                     return req.relayerWithTokenFromOriginalRequest.postAsync( "/s/"+req.params.server+"/su/docker/webhosting/"+whResponse.wh_id+"/containers/new", {
                        image: webapp,
                        start: true,
                        forceNameLabel: whResponse.wh_id+"-default",
                     })
                     .then((cre)=>{
                        var container_id = cre.result.container;
                        return req.relayerWithTokenFromOriginalRequest.postAsync( "/s/"+req.params.server+"/su/docker/webhosting/"+whResponse.wh_id+"/container/"+container_id+"/set-as-default-web", {})
                     })
                     .catch(ex=>{
                        console.error("Unable to create Docker app for webstore", ex);
                        app.InsertException(req, ex, "Docker webapp creation failed");
                     })

                 })


          })
          .then(()=>{

            // wiping the token from the cache so a new lookup will take place; this is needed so that the relayer would see the new, just added webhosting
            req.TokenUser.uncache();

            return whResponse
          })


          return req.sendPromResultAsIs(p)

     })

     .delete(userSpec, function(req,res,next){

        var webhostingInfo
        var d

        var emitter = app.commander.EventEmitter()
        return validateWebhostingsParamaters(req)
          .then(ad=>{
             d = ad
             return emitter.spawn()
          })
          .then(eh=>{

             req.sendTask(Promise.resolve(eh))

             emitter.send_stdout_ln("Querying webhosting")
             return req.relayerWithTokenFromOriginalRequest.getAsync("/s/"+req.params.server+"/webhosting/"+d.wh_id)
               .catch(ex=>{
                  if(ex.message == "PERMISSION_DENIED") {
                      emitter.send_stdout_ln("You are not authorized to remove this webstore.");
                      throw ex;
                  }

                  console.error("unable to query webhosting info ", d.wh_id, ex)
                  emitter.send_stdout_ln("We were unable to query webhosting info, proceeding anyway")
                  return Promise.resolve({result:{}})
               })

          })
         .then((whResponse)=>{

             webhosting = whResponse.result

             if(!webhosting.wh_installatron_server) return Promise.resolve()

             emitter.send_stdout_ln("Removing Installatron entries")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+webhosting.wh_installatron_server+"/su/installatron/webhosting/user", {server: req.params.server, webhosting_id: d.wh_id})
          })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{

             emitter.send_stdout_ln("Removing Docker containers")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync( "/s/"+req.params.server+"/su/docker/webhosting/"+d.wh_id+"/containers", {})
          })
         .then(dh=>{

            // note: this request is directed to the appropriate backend server via the /s/servername/ route prefix so
            // the getPipe requests here go to the correct internal microservice

            var mc = getPipe("ServersDocker")
            return emitter.relayRequest(mc.request("GET", '/docker/tasks/'+dh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{

             emitter.send_stdout_ln("Removing docroots")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync( "/s/"+req.params.server+"/su/docrootapi/cleanup/webhostings/"+d.wh_id, {})
               .catch(ex=>{
                  console.error("unable to remove docroots, proceeding", ex)
               })
          })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{

             emitter.send_stdout_ln("Removing firewall rules")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync( "/s/"+req.params.server+"/su/iptables/whextra/"+d.wh_id+"/firewallrules", {})
          })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{

             emitter.send_stdout_ln("Removing email accounts")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync( "/s/"+req.params.server+"/su/email/webhostings/"+d.wh_id, {})
          })
         .then(dh=>{

            var mc = getPipe("ServersEmail")
            return emitter.relayRequest(mc.request("GET", '/email/tasks/'+dh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{

             emitter.send_stdout_ln("Removing cron entries")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync( "/s/"+req.params.server+"/su/fileman/"+d.wh_id+"/cron", {})
          })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
             emitter.send_stdout_ln("Removing FTP accounts")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+req.params.server+"/su/ftp/sites/"+d.wh_id, {})
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
             emitter.send_stdout_ln("Removing GIT entries")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+req.params.server+"/su/git/"+d.wh_id, {})
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
             emitter.send_stdout_ln("Removing purchased certificates")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+req.params.server+"/su/cert/cleanup/"+d.wh_id, {})
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
             emitter.send_stdout_ln("Removing databases")
             return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+req.params.server+"/su/dbms/databases/"+d.wh_id, {})
         })
         .then(dh=>{

            // note: this request is directed to the appropriate backend server via the /s/servername/ route prefix so
            // the getPipe requests here go to the correct internal microservice

            var mc = getPipe("ServersDbms")
            return emitter.relayRequest(mc.request("GET", '/dbms/tasks/'+dh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
            emitter.send_stdout_ln("Uninitializing the webstore")
            return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+req.params.server+"/su/webhosting/"+d.wh_id+"/hook/uninit", {})
         })
         .then(dh=>{

            var mc = getPipe("ServersWebhosting")
            return emitter.relayRequest(mc.request("GET", '/webhosting/tasks/'+dh.result.id))
         })
         .then(()=>{
            emitter.send_stdout_ln("Ensuring the files are not readonly on the webstore")
            return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+req.params.server+"/su/fileman/"+d.wh_id+"/toreadwrite", {path_category:"web"})
         })
         .then(dh=>{

            var mc = getPipe("ServersFileman")
            return emitter.relayRequest(mc.request("GET", '/fileman/tasks/'+dh.result.id))
         })
         .then(()=>{
            emitter.send_stdout_ln("Removing the files of the webstore")
            return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+req.params.server+"/su/fileman/"+d.wh_id+"/wipe", {path_category:"web"})
         })
         .then(dh=>{

            var mc = getPipe("ServersFileman")
            return emitter.relayRequest(mc.request("GET", '/fileman/tasks/'+dh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
            emitter.send_stdout_ln("Removing the files of the mailstore (just for sure)")
            return req.relayerWithTokenFromOriginalRequest.postAsync("/s/"+req.params.server+"/su/fileman/"+d.wh_id+"/wipe", {path_category:"mail"})
         })
         .then(dh=>{

            var mc = getPipe("ServersFileman")
            return emitter.relayRequest(mc.request("GET", '/fileman/tasks/'+dh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
            emitter.send_stdout_ln("Removing webstore itself")
            return req.relayerWithTokenFromOriginalRequest.deleteAsync("/s/"+req.params.server+"/su/webhosting/"+d.wh_id, {})
         })
         .then(wh=>{
            var mc = getPipe("ServersWebhosting")
            return emitter.relayRequest(mc.request("GET", '/webhosting/tasks/'+wh.result.id))
         })
         .catch(webhostingNotFoundIsFine)
         .then(()=>{
            emitter.send_stdout_ln("Webhosting removed successfully.")
            emitter.close()
         })
         .catch(ex=>{
             console.error("could not finish the webhosting removal task", ex)
             emitter.send_stdout_ln("Removing of the webstore FAILED.")
             emitter.close(1)
         })

        function webhostingNotFoundIsFine(ex){
            console.error("we have got an error:", ex)
            if((ex.message != "WEBHOSTING_NOT_FOUND")&&(ex.message != "WEBSTORE_ROOT_DIR_NOT_FOUND")) {
               emitter.close(1)
               throw ex
            }
            return Promise.resolve()
        }


     })




  return router

  function userSpec(req,res,next) {
      return vali.async(req.body.json,
      {
         wh_user_id:   { presence: true, isString: {lazy: true} },
      }).then((d)=>{
        req.wh_user_id = d.wh_user_id
        next()
      })
  }



  function getPipe(servername) {
      var pipeConfig = app.Mapi.cloneServersAsPipe(app[servername].FirstServer())
      var mc = app.Mapi(pipeConfig)
      return mc
  }


}
