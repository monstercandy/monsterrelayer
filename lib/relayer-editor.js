module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter({mergeParams: true})

  router.use(function(req,res,next){
     if(!req.TokenUser.session.IsAnyGranted(["SUPERUSER"]))
        return next(new MError("PERMISSION_DENIED"))
     return next();
  })
  


  var pathRegexp = new RegExp('/[^/]+/(.+)');
  Array(
      "Accountapi",
      "Installatron",
      "Tapi",
      "Docrootapi",
      "Git",
      "Webhosting",
      "Iptables",
      "Dbms",
      "Eventapi",
      "Fileman",
      "Ftp",
      "Mailer",
      "Email",
      "Geoip",
      "Cert",
      "Docker",
  ).forEach(n=>{
     const supported = app["Servers"+n] ? true : false;
     router.post("/"+n.toLowerCase()+"/supported", function(req,res,next){
        return req.sendResponse({supported: supported});
     });
     if(!supported) return;

     router.all("/"+n.toLowerCase()+"*", relay);

     function relay(req,res,next){
        // req.path is something like /docrootapi/list at this point. We need to keep from the second component only
        var m = pathRegexp.exec(req.path);
        if(!m) throw new MError("INVALID_ROUTE");
        
        return app.Relay("Servers"+n, "/editor/"+m[1], req, next);
     }

  })

  return router

}
