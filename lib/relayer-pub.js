module.exports = function(app) {


  const MError = require('MonsterError')



  var router = app.ExpressPromiseRouter()

  const  grantHelper = require("grant-helper.js")(app);

var RateLimit = require("RateLimit")

var loginLimiter

var limiter = new RateLimit({
  windowMs: app.config.get("public_ratelimit_window_sec") *1000, // 15 minutes
  max: app.config.get("public_ratelimit_max"), // limit each IP to 100 requests per windowMs
});


   router.get("/captcha/expected", function(req,res,next){
   	  req.result = {"login": loginExpectsCaptcha(req), "forgotten": app.config.get("captcha_for_forgotten"), "registration":  app.config.get("captcha_for_registration")}
   	  next()
   })

   setupPubQueries()


   router.use(limiter)


   grantHelper.configureCaptchaRoutes(router);


   // this one is here for unit testing only
   router.post("/captcha", function(req,res,next){
		return req.sendOk(captchaEnforceAsync("-", req));
   })


  router.get("/account/webhostingtemplate/:template", expressHelper(function(mapi, req, res, next) {

		return mapi.searchAsync("/accountapi/webhostingtemplates", {t_name:req.params.template,t_public: true})
		  .then((regRes)=>{

		  	  if(regRes.result.length != 1)
		  	  	 throw new MError("TEMPLATE_NOT_FOUND")

			  req.result = regRes.result[0]
			  next()
		  })

  }))

	router.post("/account/registration", expressHelper(function(mapi, req, res, next) {

        return captchaEnforceAsync("captcha_for_registration", req)
          .then(()=>{
   		     return mapi.postAsync("/accountapi/accounts/registration", req.body.json)
          })
          .then(regRes=>{
          	 return req.sendResponse(regRes.result);
          })

	}))


	if(app.config.get("public_login_ratelimit_captcha_after")) {
	    loginLimiter = new RateLimit(app.config.get("public_login_ratelimit_captcha"))
	    router.use("/account/authentication", loginLimiter)

	}

	router.post("/account/authentication", expressHelper(function(mapi, req, res, next) {

		var p;
		if(loginExpectsCaptcha(req)) {
			p = captchaEnforceAsync("public_login_ratelimit_captcha_after", req)
		}
		else
			p = Promise.resolve();

		return p.then(()=>{
   		   return mapi.postAsync("/accountapi/authentication/credential", req.body.json, app.setupRequestOptions({}, req))
		})
		  .then((loginRes)=>{
	            // console.log("FOOOOO", loginRes)

	            return req.sendResponse(loginRes.result);
		  })
		  .catch((ex)=>{
	          // console.log("FAAAAAILED", ex)
	          if(ex.message == "TOTP_REQUIRED") throw ex;

			  next(new MError("AUTH_FAILED"))
		  })

	}))


	router.post("/account/forgotten/send", expressHelper(function(mapi, req, res, next) {

        return captchaEnforceAsync("captcha_for_forgotten", req)
          .then(()=>{
            	return mapi.postAsync("/accountapi/accounts/send_forgotten_password", req.body.json)

          })
		  .then((regRes)=>{
				return req.sendResponse(regRes.result);
		  })

	}))

	router.post("/account/forgotten/auth", expressHelper(function(mapi, req, res, next) {

		return mapi.postAsync("/accountapi/authentication/forgotten", req.body.json)
		  .then((regRes)=>{
				req.result = regRes.result
				next()
		  })
		  .catch((ex)=>{
	          // console.log("FAAAAAILED", ex)
			  next(new MError("AUTH_FAILED"))
		  })


	}))


	router.post("/account/email/verify", expressHelper(function(mapi, req, res, next) {
		return mapi.postAsync("/accountapi/account/"+req.body.json.account_id+"/email/"+req.body.json.email+"/verify", req.body.json)
		  .then((regRes)=>{
				req.result = regRes.result
				next()
		  })

	}))



	return router


   function loginExpectsCaptcha(req) {
   	  return ((loginLimiter)&&(loginLimiter.query(req.clientIp)>=app.config.get("public_login_ratelimit_captcha_after"))) ? true : false

   }

   function expressHelper(cb){
   	  return function(req,res,next){
		    var mapi = app.getMapiPoolAndSetupRequest("ServersAccountapi", req);

			return cb(mapi, req, res, next)
   	  }
   }

   function captchaEnforceAsync(name, req){
   	    return grantHelper.captchaEnforceAsync(name, req);
   }


   function setupPubQueries(){
       "use strict"
       var m = {
       	  "/account/languages":"/languages",
       	  "/account/loginlog/subsystems":"/config/loginlog/subsystems",
       	  "/account/callback_request":"/config/callback_request",
       	  "/account/country_codes":"/tel/country_codes",
       	  "/account/email_categories":"/email/categories",
       }

       for(let q of Object.keys(m)) {
			router.get(q, expressHelper(function(mapi, req, res, next) {
				let v = m[q]

				return mapi.getAsync("/accountapi"+v)
				  .then((regRes)=>{
						return req.sendResponse(regRes.result);
				  })

			}))

       }

   }

}
