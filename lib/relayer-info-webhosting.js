/*
requests are granted for some specific info requests only
*/

module.exports = function(app) {

  const MError = require('MonsterError')

  var router = app.ExpressPromiseRouter()


  router.get("/:webhosting_id", webhostingInfoBusinessLogic)
  router.get("/:webhosting_id/bandwidth-limited", webhostingInfoBusinessLogic)


  router.use(function(req,res,next){

      if(!req.granted) return next(new MError("PERMISSION_DENIED"))

      var aPath = "/webhosting/" + req.path

      app.Relay("ServersWebhosting", aPath, req, next)

  })


  return router


  function infoBusinessLogic(role, req,res,next) {
      if(req.TokenUser.session.IsGranted(role)) req.granted = true

      next()
  }


  function webhostingInfoBusinessLogic(req,res,next) {
      infoBusinessLogic("INFO_WEBHOSTING", req,res,next)
  }

}
