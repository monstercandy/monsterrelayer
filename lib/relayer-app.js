// permissions needed (where the accountapi is not on the same host): ["INFO_ACCOUNT","INFO_EMAIL",INFO_WEBHOSTING_TEMPLATE","INFO_SERVERS","INFO_WEBHOSTING","INFO_SESSION","ESCALATE_EXCEPTION","LOGINLOG_EVENT_REPORT", "SEND_EMAIL_TO_USER"]
module.exports = function(moduleOptions) {
  moduleOptions = moduleOptions || {}
  moduleOptions.subsystem = "relayer"

  var config = require('MonsterConfig');

  var me = require('MonsterExpress');
  const MError = me.Error


  config.defaults({
     "timeout": 30000,
     "sync_timeout": 60000,
     "public_ratelimit_window_sec": 15*60,

     // the site is not yet SPA, so we expect many captcha expected queries
     // TODO: number of login requests should have a dedicated/different limit than this
     "public_ratelimit_max": 200,

     "time_machine_active": true,
     "time_machine_cleanup_older_than_days": 3,
     "cron_time_machine_cleanup": "30 0 * * *",
     "time_machine_services": ["dbms"],

     // this is about the telephone number verification stuff:
     "account_ratelimit_window_sec": 15*60,
     "account_ratelimit_max": 5,

     "cron_wie_backup": "5 0 * * *",

     "captcha_case_sensitive": false,

     "captcha_characters": 6,
     "captcha_memcache": {cleanupIntervalMs: 5000, cleanupOlderSec: 3600},

     "cache_session_info_seconds": 15*60, // we cache the sessions for 15 minutes
     "cleanup_session_info_ms": 10*60*1000, // we cleanup them every 10 minutes

     "captcha_for_forgotten": true,
     "captcha_for_registration": true,

     "tapi_non_privileged_soa_edit": false,

     "public_login_ratelimit_captcha_after": 0,
     "public_login_ratelimit_captcha": {  windowMs: 15 *1000, max: 100 }, // limit each IP to 100 requests per windowMs

     "email_service_account_tld": ".svc",
     "email_service_account_username": "mailer",

     "servers_info_directory": "memory",
     "cache_servers_info_seconds": 15*60, // cache the response for 15 minutes

  })


  if(!config.get("server_name")) throw new Error("Server name not configured")
  if(!config.get("service_primary_domain")) throw new Error("Service primary domain not configured")

  const server_name = config.get("server_name");

  config.tokenapi = true
  config.appRestPrefix = "/api"

  const mcForwardedForSecret = config.get("mc_forwarded_for_secret");

  var app = me.Express(config, moduleOptions);
  app.enable("trust proxy")  // this application receives x-forwarded-for headers from nginx

  var router = app.PromiseRouter(config.appRestPrefix)


  var commanderOptions = app.config.get("commander") || {}
  commanderOptions.routerFn = app.ExpressPromiseRouter
  app.commander = require("Commander")(commanderOptions)

  app.SetupRouterForPipeTasks = function(router, serverName, serverPrefix, additionalRegexps) {
    var regexps = [/^\/tasks\//];
    if(additionalRegexps)
      regexps = regexps.concat(additionalRegexps);
    Array("get","post").forEach(method=>{
      regexps.forEach(regexp=>{
        router[method](regexp, function(req,res,next){
            doPiping(req,res,app[serverName].FirstServer(), serverPrefix)
        })
      })
    })
  }

  var mclientPoolCache = {}
  app.GetMapiPool = function(name, poolOptions) {
     if(!poolOptions) poolOptions = {};
     if((!poolOptions.dontCache)&&(mclientPoolCache[name])) return mclientPoolCache[name];

     if(!app[name]) throw new MError("INTERNAL_ERROR", "no server configured: "+name);

     if(!poolOptions.config) poolOptions.config = config;

     var mc = app.Mapi.getMapiPool(app[name], poolOptions);
     if(!poolOptions.dontCache) mclientPoolCache[name] = mc;
     return mc;
  }

  app.getMapiPoolAndSetupRequest = function(serverRouterName, req, forceRelayer, additionalPoolOptions) {
      var mapi = app.GetMapiPool(serverRouterName, extend({}, {forceRelayer: forceRelayer}, additionalPoolOptions));
      mapi.SetupFieldsByRequest(req, mcForwardedForSecret);
      return mapi;
  }

  const MonsterInfoLib = require("MonsterInfo")
  app.ServersInfoAccountapi = router.ServersMiddleware("info-accountapi", 0)
  app.MonsterInfoSession = MonsterInfoLib.session({config: app.config, accountServerRouter: app.ServersInfoAccountapi})

  // app.ServersInfoAccountapi is a prereq of serverslib:
  app.ServersLib = require("lib-servers.js")(app)

  app.ServersAccountapi = router.ServersMiddleware("accountapi", 0)
  app.ServersTapi = router.ServersMiddleware("tapi", 0)
  app.ServersGit = router.ServersMiddleware("git", 0)
  app.ServersFtp = router.ServersMiddleware("ftp", 0)
  app.ServersMailer = router.ServersMiddleware("mailer", 0)
  app.ServersEmail = router.ServersMiddleware("email", 0)
  app.ServersCert = router.ServersMiddleware("cert", 0)
  app.ServersInstallatron = router.ServersMiddleware("installatron", 0)
  app.ServersDocrootapi = router.ServersMiddleware("docrootapi", 0)
  app.ServersWebhosting = router.ServersMiddleware("webhosting", 0)
  app.ServersDbms = router.ServersMiddleware("dbms", 0)
  app.ServersFileman = router.ServersMiddleware("fileman", 0)
  app.ServersIptables = router.ServersMiddleware("iptables", 0)
  app.ServersDocker = router.ServersMiddleware("docker", 0)

  app.pub = require("relayer-pub.js")(app) // unit tests rely on app.pub
  router.use("/pub", relayToOtherServersIfNeeded("accountapi"), app.pub)

  var taskPathes = new RegExp('^/api/s/[^/]+/(dbms|docrootapi|docker|email|fileman|git|webhosting|iptables|cert)/tasks/')
  router.RegisterDontParseRequestBody(taskPathes)
  router.RegisterPublicApis(taskPathes)

  router.RegisterPublicApis(new RegExp('^/api/s/[^/]+/email/pub/'))

  var shellinabox = new RegExp('^/api/s/[^/]+/docker/shellinabox/');
  router.RegisterPublicApis(shellinabox)
  router.RegisterDontParseRequestBody(shellinabox)

  var localTaskPath = new RegExp('^/api/s/[^/]+/tasks/')
  router.RegisterDontParseRequestBody(localTaskPath)
  router.RegisterPublicApis(localTaskPath)

  // others
  router.RegisterPublicApis("/pub")
  router.RegisterPublicApis(new RegExp('^/api/s/[^/]+/git/triggers/')) // this is needed for github callbacks

  router.use("/tapi", relayToOtherServersIfNeeded("tapi"), require("relayer-tapi.js")(app))
  router.use("/my", require("relayer-my.js")(app));
  router.use("/account", relayToOtherServersIfNeeded("accountapi"), require("relayer-account.js")(app))
  router.use("/accountapi", require("relayer-accountapi-misc.js")(app))

  router.use("/info/account", require("relayer-info-account.js")(app))

  // note!: this route is here to aid unit tests. relayToOtherServersIfNeeded is there purposefully!
  router.use("/test", relayToOtherServersIfNeeded("tapi"), require("relayer-test.js")(app))


  var SuLib = require("relayer-su.js")
  SuLib(app, router, "accountapi", "ServersAccountapi", ["ADMIN_ACCOUNTAPI"], relayToOtherServersIfNeeded);
  SuLib(app, router, "tapi", "ServersTapi", ["ADMIN_TAPI"], relayToOtherServersIfNeeded);

  router.use("/wizard", require("relayer-wizard.js")(app))

  router.use("/su/sync", require("relayer-sync-server.js")(app))

  // note the reason why we do it this way as for most of the requests the payload is already processed by our json body reader. exception is the tasks processing
  router.use("/tasks", app.commander.router)
  router.use("/s/:server/tasks", rerouteToOtherServerIfNeeded("/tasks"), app.commander.router)

  router.use("/s/:server", rerouteToOtherServerIfNeeded(""), require("relayer-specific-server-only.js")(app))

  router.get("/su/servers/:role?", function(req,res,next){

       var userFilter = serversAuthorizationLogic(req);

       return app.ServersLib.GetServers(req.params.role, false, userFilter)
          .then(servers=>{
             req.sendResponse(servers.map(function(q){ return q.s_name }));
          })

  })
  router.search("/su/servers/", function(req,res,next){
       var userFilter = serversAuthorizationLogic(req);

       return req.sendPromResultAsIs(app.ServersLib.GetServers(null, false, userFilter))
  })

  function serversAuthorizationLogic(req){
      var userFilter;
      if(!req.TokenUser.session.IsAnyKindOfAdmin()) {

         if(!req.TokenUser.session.IsGranted("SERVER_OWNER"))
             throw new MError("PERMISSION_DENIED")

         // he/she is a server owner
         userFilter = req.TokenUser.account.u_id;
      }

      return userFilter;
  }

  router.use("/su/flush", require("relayer-flush.js")(app))





   // we need this one becuase of session keepalive support (this is not the same /ping what is implemented in MosnterExpress level)
   router.get("/ping", function(req,res,next){
       req.result = "pong"
       next()
   })

  // this call should be relayed towards each business logics (all servers configured)
  router.post("/logout", relayToOtherServersIfNeeded("accountapi"), function(req,res,next){

    var token = app.DeleteTokenFromCache(req);

  	var mapi = app.getMapiPoolAndSetupRequest("ServersAccountapi", req);

  	return mapi.deleteAsync("/accountapi/token/"+token, req.body.json)
  	  .then(()=>{
  	  	 req.result = "ok"
  	  	 next()
  	  })

  })


  app.setupRequestOptions = function(requestOptions, req){
      return extend({}, requestOptions, {
        SetupFieldsByRequest: req, 
        forwardedForSecret: mcForwardedForSecret,
      });
  }


  function requestLogic(serverRouterName, aPath, req, customPayload, mapi) {

      // for requets towards internal servers we dont have the /s/s1 prefix
      // but for public server ones we need them to be prefixed once again

      if((!req.noUriTransform)&&(req.params.server)&&(mapi.getApiType() == "public"))
        aPath = "/s/"+req.params.server+"/"+aPath

      var x = app.setupRequestOptions({
          method: req.method,
          uri: aPath,
          payload: customPayload || req.body.json
        }, 
        req
      );
      // console.log("relaying:", x)
      return x
  }


  app.Relay = function(serverRouterName, aPath, req, next, customPayload, forceRelayer){

    var mapi = app.getMapiPoolAndSetupRequest(serverRouterName,req, forceRelayer)
    mapi.request(requestLogic(serverRouterName, aPath, req, customPayload, mapi),
        (err, result, poolErrors) => {
            if(err) return next(err)


            if((poolErrors)&&(poolErrors.length)) {

				 poolErrors.forEach(ex=>{
					app.InsertException(req, ex, "relaying failed for "+ex.server.host)
				 })

            }

            req.result = result
            next()
        })

  }


  app.Cleanup = function() {


      config.saveVolume({"api_secret": config.getApiSecret()})

      const path = require("path");

      var patternsToDel = [];
      patternsToDel.push(path.join(app.getWieDirectory(), "*.json"));
      patternsToDel.push(path.join(app.getTimeMachineDirectory(), "*.json"));

      const del = require('MonsterDel');
      // console.log("xxx",patternsToDel)
      return del(patternsToDel,{"force": true})

  }


  app.DeleteTokenFromCache=function(req, returnPromise){
      var token = req.get("MonsterToken")
      var prom = app.MonsterInfoSession.Delete(token);
      if(returnPromise)
         return prom.then(()=>{return token;});
      return token
  }


  app.constructUrlFromServer=function(server){
     return server.scheme+"://"+server.host+":"+server.port+(server.api_prefix||"")
  }
  app.getLocalRelayerParams = function(){
     return {"host":"127.0.0.1", "port": app.config.get("listen_port"), "scheme": "http", api_prefix: app.config.appRestPrefix}
  }
  app.getLocalRelayerUrlPrefix = function(){
     return app.constructUrlFromServer(app.getLocalRelayerParams());
  }
  app.getMapiRelayer= function(req) {
    const MApiLib = require("MonsterClient")

    var mapiparams = app.getLocalRelayerParams();
    mapiparams.auth_token = req.token
    var mapi = MApiLib(mapiparams, app.config)
    return mapi

  }

  app.getOriginatingServer = function(req) {
      return ((req.TokenUser || {}).session || {}).originatingServer || req.clientServer || server_name;
  }
  app.assignOriginatingServer = function(req){
      req.body.json.server = app.getOriginatingServer(req);
  }

  app.getWieDirectory = function(){
     const path = require("path");
     var wie_dir = app.config.get("wie_directory") || path.join(app.config.get("volume_directory"), "wie");
     return wie_dir;
  }
  app.getTimeMachineDirectory = function(){
     const path = require("path");
     var wie_dir = app.config.get("wie_directory") || path.join(app.config.get("volume_directory"), "tm");
     return wie_dir;
  }

  app.getWebhostingDomainsRaw = function(req) {
     var relayerWithTokenFromOriginalRequest = app.getMapiRelayer(req)
     var re;
     return relayerWithTokenFromOriginalRequest.getAsync("/s/[this]/email/webhostings/"+req.params.webhosting+"/domains/")
       .then(emailsResult=>{
           re = emailsResult.result.domains.map(x => x.do_domain_name);
           return relayerWithTokenFromOriginalRequest.getAsync("/s/[this]/docrootapi/docroots/"+req.params.webhosting)
       })
       .then(docrootsResult=>{
           docrootsResult.result.forEach(dn=>{
              var i = re.indexOf(dn);
              if(i < 0)
                re.push(dn);
           })

           return re;

       })    
  }


  return app


  function relayToOtherServersIfNeeded(filterRole) {
     return function(req,res,next) {

        // TODO: review the implementation regarding the backend service initialization question


        // we don't simply call next() for GET requests, since the expected server might be anywhere else (GetRelayerPoolWithoutUs will return empty response)

        if((req.TokenUser)&&(req.TokenUser.derived_mode))
            return next() // this request was relayed directly to us! (the token has already been verified via MonsterExpress/MonsterInfoSession)

         // this means we have a primary token, we might need to relay the request to other servers

         // this means the request was not meant directly for us, we need to relay it towards all interested parties (maybe us as well)
         return app.ServersLib.GetServers(filterRole)
            .then(servers=>{
               // if(!servers.length) return next() // as a last effort, we try to handle the request locally (will break "post, there are NO servers, can NOT serve locally")

               var pool = app.ServersLib.GetRelayerPoolWithoutUs(servers, req);
               /*
               Conditions:
               - there is anything to relay to
               - we cannot serve it locally
               - it is not a GET
               */
               if(pool.length > 0) {

                   if((!pool.doCallNext) || (req.method != "GET"))
                       relayViaPool(pool, req, res, next, !pool.doCallNext)

               }

               if(pool.doCallNext)
                 return next();

               if(pool.length > 0)
                 return; // final result will come from some of the remote servers


               throw new MError("INTERNAL_ERROR", "nothing seems to be configured");
            })


     }
  }

  function rerouteToOtherServerIfNeeded(suffixForPiping){
      return function(req,res,next){

            // console.log("!!!!!!", req.path); process.reallyExit(); // -> "/webhosting/1234/firewallrules"

            if("[this]"== req.params.server)
               req.params.server = server_name;

            // was it meant to us?
            if(server_name == req.params.server)
               return next()

            // otherwise lets try to relay it
            return app.ServersLib.GetServerByName(req.params.server)
              .then(server=>{

                 var pool = app.ServersLib.GetRelayerPoolWithoutUs([server], req) // dontParseBody is configured in MonsterExpress for RegisterDontParseRequestBody stuffs

                 if(req.dontParseBody)
                    doPiping(req,res,pool.GetRawServers()[0], "/api/s/[this]"+suffixForPiping)
                 else
                    relayViaPool(pool, req, res, next, true)

              })

      }
  }

  function relayViaPool(pool, req, res, next, doSendResponse) {

       return pool.requestAsync(
         app.setupRequestOptions({method: req.method, uri: req.originalUrl, payload: req.body.json}, req)
       )
         .then(r=>{
              // token cache hacks
              if(req.originalUrl.startsWith("/api/account/webhostingdomains")) {
                 console.log("Invalidating cache as the permissions might have probably changed");
                 return app.DeleteTokenFromCache(req, true)
                  .then(()=>{
                     return r;
                  })
              }

              return r;
         })
         .then(r=>{

              if(doSendResponse) // this means the final result comes from relaying
                app.sendResponse(req, res, 200, r.result)

         })
         .catch(x=>{
              if(doSendResponse)
                return next(x);

              console.error("Error while relaying message", req.originalUrl, x)
         })

  }

  function doPiping(req,res,serverParams, serverPrefix) {
          var pipeConfig = app.Mapi.cloneServersAsPipe(serverParams)
          var mc = app.Mapi(pipeConfig)
		  var destReq = mc.request(req.method, serverPrefix+req.path)
          destReq.on('error', function(err) {
			  console.error("error while piping towards", serverParams, err);
		  });
          req.pipe(destReq).pipe(res).on("error", function(err) {
			  console.error("error while pipeing data back to receiver; source", serverParams, err);
		  });

  }
}

