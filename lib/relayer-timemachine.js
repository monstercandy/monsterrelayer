
module.exports = function(app) {



      const subsystems = {

          // note: webhosting shall be the first, as it must be restored at first (other subsystems rely on it)
          "dbms": "ServersDbms",
          "ftp": "ServersFtp",

      };
      const subsystemNames = Object.keys(subsystems);

      const writeFileOptions = {mode: 0o600};

      const vali = require("MonsterValidators").ValidateJs()
      const MError = require('MonsterError')

      const path = require('path');
      const dotq = require('MonsterDotq');
      const fs = dotq.fs();
      const moment = require("MonsterMoment");

      var router = app.ExpressPromiseRouter();

      var activeServices = app.config.get("time_machine_services");
      


      var lastQuery = {};
      var intervals = {};
      subsystemNames.forEach(n=>{
         var i = app.config.get("time_machine_"+n+"_interval_sec") || 60;
         intervals[n] = i;
      })

      router.use(function(req,res,next){
          if(!req.TokenUser.session.IsAnyGranted(["SUPERUSER", "ADMIN_TIMEMACHINE"])) {

            if(req.TokenUser.session.IsGranted("SERVER_OWNER")) {
               var serverWanted = req.params.server || this_server_name;
               if(req.TokenUser.servers.indexOf(serverWanted) > -1)
                  return next();
            }
        
            return next(new MError("PERMISSION_DENIED"))
          }

          return next();
      });


      router.get("/config/subsystems", function(req,res,next){
          return req.sendResponse(subsystemNames);
      })

      router.get("/list/", function(req,res,next){
          return req.sendPromResultAsIs(
              listFiles()
          )
      });

      router.get("/fetch/:timestamp", function(req,res,next){
          if(!req.params.timestamp.match(/^[0-9]+$/))
             throw new MError("INVALID_TIMESTAMP");

          var tmDir = app.getTimeMachineDirectory();
          var f = path.join(tmDir, req.params.timestamp+".json");

          return req.sendPromResultAsIs(
             fs.readFileAsync(f, "utf-8")
               .then(f=>{
                  return JSON.parse(f);
               })
          );
      })

      router.post("/cleanup", function(req,res,next){
         return req.sendOk(doCleanup());
      })

      if(app.config.get("time_machine_active")) {
          install_cron();
          install_logic();
      }

      return router;

      function doCleanup() {
         var tmDir = app.getTimeMachineDirectory();
         var now = moment.nowUnixtime();
         var secondsOlder = app.config.get("time_machine_cleanup_older_than_days") * 86400;
         return listFiles()
           .then(files=>{
               return dotq.linearMap({array: files, action: function(f){
                  var d = parseInt(f, 10);
                  if(now - d < secondsOlder)
                     return;

                  var full = path.join(tmDir, f+".json");
                  return fs.unlinkAsync(full)
               }})
           })
      }

      function listFiles(asIs){
         var tmDir = app.getTimeMachineDirectory();
         var re = [];
         return fs.readdirAsync(tmDir)
           .then(files =>{
              if(asIs) return files;

              var r = new RegExp('^([0-9]+)\.json$');
              files.forEach(file=>{
                  var m = r.exec(file);
                  if(!m) return;

                  var wh_id = m[1];
                  re.push(wh_id);
              })
              return re;
           })
      }

      function install_logic(){
        if(app.tmLogic) return;

        var mapis = {};
        activeServices.forEach(serviceName=>{
           var serverName = subsystems[serviceName];
           var mapi = app.Mapi.getMapiPool(app[serverName], {dontCache: true, config: app.config});
           mapi.dontCache = true;
           mapis[serviceName] = mapi;
        });
 
        app.tmLogic = setInterval(function(){
            var now = moment.nowUnixtime();

            var l = {};
            return dotq.linearMap({array: activeServices, catch: true, action: function(serviceName){
                var lastCheck = lastQuery[serviceName];
                var i = intervals[serviceName];
                if((lastCheck)&&(lastCheck > now - i)) return;

                lastQuery[serviceName] = now;

                var serverName = subsystems[serviceName];
                var serversRouter = app[serverName];
                var servers = serversRouter.GetRawServers();
                if(!servers.length) return;

                var mapi = mapis[serviceName];
                return mapi.getAsync("/"+serviceName+"/tm")
                  .then(x=>{
                     // nothing to report?
                     if(!x.result) return;

                     l[serviceName] = x.result;
                  })
            }})
            .then(()=>{
               if(Object.keys(l).length <= 0) return;

               // and now lets save it
               var s = JSON.stringify(l);
               var tmDir = app.getTimeMachineDirectory();
               var full = path.join(tmDir, now+".json");
               return fs.writeFileAsync(full, s);
            })
            
        }, 1000);
      }


      function install_cron(){
        if(app.cron_time_machine_cleanup) return;
        app.cron_time_machine_cleanup = true;

        var csp = app.config.get("cron_time_machine_cleanup")
        if(!csp) return

        const cron = require('Croner');

        cron.schedule(csp, function(){
           console.log("tm cleanup");
           return doCleanup()
            .catch(ex=>{
                console.error("Unable to cleanup time machine files", ex)
            })

        });
      }

       function toJsonStr(a){
          return JSON.stringify(a, null, 4);
       }

}
