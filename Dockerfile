FROM monstercommon

ADD lib /opt/MonsterRelayer/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterRelayer/lib/bin/relayer-install.sh && /opt/MonsterRelayer/lib/bin/relayer-test.sh

ENTRYPOINT ["/opt/MonsterRelayer/lib/bin/relayer-start.sh"]
